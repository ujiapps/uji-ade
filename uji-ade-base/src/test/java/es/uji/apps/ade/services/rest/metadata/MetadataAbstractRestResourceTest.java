package es.uji.apps.ade.services.rest.metadata;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import es.uji.apps.ade.services.rest.AbstractRestResourceTest;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MetadataAbstractRestResourceTest extends AbstractRestResourceTest
{
    protected Map<String, Map<String, String>> retrieveMetadataFrom(String reference)
    {
        ClientResponse response = resource.path("/metadata/" + reference)
                .header("X-UJI-AuthToken", authToken)
                .type(MediaType.APPLICATION_FORM_URLENCODED)
                .get(ClientResponse.class);

        return response.getEntity(Map.class);
    }

    protected Map addMetadataToSampleImage(String reference, Map<String, List<String>> params)
    {
        MultivaluedMap<String, String> properties = new MultivaluedMapImpl();
        properties.putAll(params);

        ClientResponse response = resource.path("/metadata/" + reference)
                .header("X-UJI-AuthToken", authToken)
                .type(MediaType.APPLICATION_FORM_URLENCODED)
                .put(ClientResponse.class, properties);

        return response.getEntity(Map.class);
    }

    protected Map<String, List<String>> generalMetadataSet(String reference)
    {
        Map<String, List<String>> metadata = new HashMap<>();
        metadata.put("author", Arrays.asList("borillo"));
        metadata.put("reference", Arrays.asList(reference));

        return metadata;
    }

    protected Map<String, List<String>> multiValueMetadatSet()
    {
        Map<String, List<String>> metadata = new HashMap<>();
        metadata.put("author", Arrays.asList("borillo", "rubert"));

        return metadata;
    }
}
