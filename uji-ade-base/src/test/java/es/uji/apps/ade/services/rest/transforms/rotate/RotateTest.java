package es.uji.apps.ade.services.rest.transforms.rotate;

import es.uji.apps.ade.core.media.RawImage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import java.io.FileInputStream;

import static es.uji.apps.ade.services.rest.matchers.DocumentStorageMatcher.storedAt;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = false)
public class RotateTest extends RotateAbstractRestResourceTest
{
    @Test
    public void shouldRotateDegreesAmount() throws Exception
    {
        String reference = createSampleImage();

        RawImage rotatedImage = callRotateResource(reference, NEW_DEGREES);

        RawImage originalImage = new RawImage(new FileInputStream("src/test/resources/prueba.jpg"));

        assertThat(rotatedImage.getWidth(), is(originalImage.getHeight()));
        assertThat(rotatedImage.getHeight(), is(originalImage.getWidth()));
        assertThat(reference + ".R", is(not(storedAt(STORAGE_PATH))));
    }
}