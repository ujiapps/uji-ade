package es.uji.apps.ade.services.rest.matchers;

import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class NoContentResponseMatcher extends TypeSafeMatcher<ClientResponse>
{
    @Override
    public boolean matchesSafely(ClientResponse response)
    {
        return (response != null && response.getStatus() == ClientResponse.Status.NO_CONTENT.getStatusCode());
    }

    public void describeTo(Description description)
    {
        description.appendText("not a HTTP 204 no content response");
    }

    @Factory
    public static <T> Matcher<ClientResponse> noContentClientResponse()
    {
        return new NoContentResponseMatcher();
    }
}