package es.uji.apps.ade.services.rest.storage;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.ade.services.rest.AbstractRestResourceTest;
import es.uji.commons.rest.json.UIEntityListMessageBodyReader;
import es.uji.commons.rest.json.UIEntityMessageBodyReader;
import es.uji.commons.rest.json.UIEntityMessageBodyWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static es.uji.apps.ade.services.rest.matchers.ClientErrorResponseMatcher.errorClientResponse;
import static es.uji.apps.ade.services.rest.matchers.DocumentStorageMatcher.storedAt;
import static es.uji.apps.ade.services.rest.matchers.NoContentResponseMatcher.noContentClientResponse;
import static es.uji.commons.testing.hamcrest.ClientOkResponseMatcher.okClientResponse;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = false)
public class StorageResourceTest extends AbstractRestResourceTest
{
    private static final String CUSTOM_REF = "ABCDEFGHIJKLMN";
    private static final String NON_EXISTING_REF = "NON_EXISTING_REF";
    private static final String EMPTY_REF = "";

    @Test
    public void shouldFailWhenNoReferenceProvided()
    {
        ClientResponse response = retrieveDocumentWithRef(EMPTY_REF);

        assertThat(response, is(errorClientResponse()));
    }

    @Test
    public void shouldFailIfReferenceDoesNotExists()
    {
        ClientResponse response = retrieveDocumentWithRef(NON_EXISTING_REF);

        assertThat(response, is(errorClientResponse()));
    }

    @Test
    public void shouldBeAbleToInsertDocuments() throws Exception
    {
        ClientResponse response = addDocument(buildSampleDocument());
        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        assertThat(response, is(okClientResponse()));
        assertThat(responseMessage.path("success").asBoolean(), is(true));
        assertThat(payload.path("fileName").asText(), is(equalTo("prueba.jpg")));
        assertThat(payload.path("reference").asText().isEmpty(), is(false));
    }

    @Test
    public void shouldBeAbleToInsertDocumentsProvidingAnExistingReference() throws Exception
    {
        ClientResponse response = addDocument(buildSampleDocument(CUSTOM_REF));
        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        assertThat(response, is(okClientResponse()));
        assertThat(responseMessage.path("success").asBoolean(), is(true));
        assertThat(payload.path("fileName").asText(), is(equalTo("prueba.jpg")));
        assertThat(payload.path("reference").asText(), is(CUSTOM_REF));
    }

    @Test
    public void shouldFailWhenDeletingANonExistentDocument() throws Exception
    {
        ClientResponse response = deleteDocumentWithRef(NON_EXISTING_REF);

        assertThat(response, is(errorClientResponse()));
    }

    @Test
    public void shouldBeAbleToDeleteAnExistentDocument() throws Exception
    {
        String reference = createSampleImage();

        ClientResponse deleteResponse = deleteDocumentWithRef(reference);

        assertThat(deleteResponse, is(noContentClientResponse()));
        assertThat(reference, is(not(storedAt(STORAGE_PATH))));
    }

    @Test
    public void shouldBeAbleToAddWatermark() throws Exception {
        String WATERMARK = "NEW VALUE";

        ClientResponse response = addDocument(buildSampleDocument());
        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        String reference = payload.path("reference").asText();
        response = updateWatermark(reference, WATERMARK);
        assertThat(response.getStatus(), is(Response.Status.NO_CONTENT.getStatusCode()));
    }
}