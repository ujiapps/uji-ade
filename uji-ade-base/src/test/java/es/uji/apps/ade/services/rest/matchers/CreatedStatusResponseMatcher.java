package es.uji.apps.ade.services.rest.matchers;

import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class CreatedStatusResponseMatcher extends TypeSafeMatcher<ClientResponse>
{
    @Override
    public boolean matchesSafely(ClientResponse response)
    {
        return (response != null && response.getStatus() == ClientResponse.Status.CREATED.getStatusCode());
    }

    public void describeTo(Description description)
    {
        description.appendText("not a HTTP 201 created response");
    }

    @Factory
    public static <T> Matcher<ClientResponse> createdStatus()
    {
        return new CreatedStatusResponseMatcher();
    }
}