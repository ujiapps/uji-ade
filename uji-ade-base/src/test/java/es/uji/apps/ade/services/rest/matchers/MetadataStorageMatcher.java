package es.uji.apps.ade.services.rest.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MetadataStorageMatcher extends TypeSafeMatcher<String>
{
    private String storagePath;

    public MetadataStorageMatcher(String storagePath)
    {
        this.storagePath = storagePath;
    }

    @Override
    public boolean matchesSafely(String reference)
    {
        Path path = Paths.get(storagePath, reference.substring(0, 2), reference.substring(2, 4), reference + ".meta");
        return Files.exists(path);
    }

    public void describeTo(Description description)
    {
        description.appendText("associated metadata doen't exist in the " + storagePath + " storage");
    }

    @Factory
    public static <T> Matcher<String> metadataStoredAt(String storagePath)
    {
        return new MetadataStorageMatcher(storagePath);
    }
}