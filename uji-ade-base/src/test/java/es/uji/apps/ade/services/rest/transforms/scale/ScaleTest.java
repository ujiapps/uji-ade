package es.uji.apps.ade.services.rest.transforms.scale;

import es.uji.apps.ade.core.media.RawImage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import static es.uji.apps.ade.services.rest.matchers.DocumentStorageMatcher.storedAt;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = false)
public class ScaleTest extends ScaleAbstractRestResourceTest
{
    @Test
    public void shouldScaleWidth() throws Exception
    {
        String reference = createSampleImage();

        RawImage image = callScaleResource(reference, NEW_WIDTH_SCALE, "w");

        assertThat(image.getWidth(), is(NEW_WIDTH_SCALE));
        assertThat(reference + ".S", is(not(storedAt(STORAGE_PATH))));
    }

    @Test
    public void shouldScaleHeight() throws Exception
    {
        String reference = createSampleImage();

        RawImage image = callScaleResource(reference, NEW_HEIGHT_SCALE, "h");

        assertThat(image.getHeight(), is(NEW_HEIGHT_SCALE));
        assertThat(reference + ".S", is(not(storedAt(STORAGE_PATH))));
    }
}