package es.uji.apps.ade.services.rest.transforms.rotate;

import com.sun.jersey.api.client.ClientResponse;
import es.uji.apps.ade.core.media.RawImage;
import es.uji.apps.ade.services.rest.AbstractRestResourceTest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class RotateAbstractRestResourceTest extends AbstractRestResourceTest
{
    protected static final int NEW_DEGREES = -90;

    protected RawImage callDefaultRotateResource(String reference) throws IOException
    {
        ClientResponse response = resource.path("/storage/" + reference)
                .queryParam("t", "ROTATE")
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        InputStream dataStream = response.getEntity(InputStream.class);

        return new RawImage(dataStream);
    }

    protected RawImage callRotateResource(String reference, int degrees) throws IOException
    {
        ClientResponse response = resource.path("/storage/" + reference)
                .queryParam("t", "ROTATE,d=" + degrees)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        InputStream dataStream = response.getEntity(InputStream.class);

        return new RawImage(dataStream);
    }
}
