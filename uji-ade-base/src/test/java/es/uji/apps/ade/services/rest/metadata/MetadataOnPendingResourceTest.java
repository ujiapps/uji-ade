package es.uji.apps.ade.services.rest.metadata;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class MetadataOnPendingResourceTest extends MetadataAbstractRestResourceTest
{
    @PersistenceContext
    private EntityManager entityManager;

    @Test
    @Transactional
    public void shouldDefineMetadataToExistingDocuments() throws Exception
    {
        String reference = createSampleImage();

        attachMetadata(reference);

        Map<String, Map<String, String>> storedMetadata = retrieveMetadataFrom(reference);
        Map<String, String> data = storedMetadata.get("data");

        assertThat(data.size(), is(7));
        assertThat(data.get("author"), is("borillo"));
        assertThat(data.get("reference"), is(reference));
    }

    @Test
    @Transactional
    public void shouldOverwriteMetadata() throws Exception
    {
        String reference = createAndStoreSampleImage();

        Map values = overwriteExtendedMetadataSet(reference);

        entityManager.flush();
        entityManager.clear();

        documentPersister.savePendingDocumentsToStorage();

        Path path = Paths.get(STORAGE_PATH, reference.substring(0, 2), reference.substring(2, 4), reference);
        InputStream inputStream = Files.newInputStream(path);

        assertThat(values.size(), is(3));

        values = attachMetadata(reference);
        assertThat(values.size(), is(2));

        Map<String, Map<String, String>> storedMetadata = retrieveMetadataFrom(reference);
        Map<String, String> data = storedMetadata.get("data");

        assertThat(data.size(), is(8));
        assertThat(data.get("author"), is("borillo"));
        assertThat(data.get("reference"), is(reference));
    }

    @Test
    @Transactional
    public void shouldAllowKeysWithMultipleValues() throws Exception
    {
        String reference = createSampleImage();

        Map values = overwriteWithMultivalueMetadataSet(reference);
        assertThat(values.size(), is(1));

        Map response = retrieveMetadataFrom(reference);
        Map storedMetadata = (Map) response.get("data");
        List<String> author = (List<String>) storedMetadata.get("author");

        assertThat(author, hasItem("borillo"));
        assertThat(author, hasItem("rubert"));
    }

    private Map overwriteWithMultivalueMetadataSet(String reference)
    {
        Map<String, List<String>> metadata = multiValueMetadatSet();
        return addMetadataToSampleImage(reference, metadata);
    }

    private Map attachMetadata(String reference)
    {
        Map<String, List<String>> metadata = generalMetadataSet(reference);
        return addMetadataToSampleImage(reference, metadata);
    }

    private Map overwriteExtendedMetadataSet(String reference)
    {
        Map<String, List<String>> metadata = generalMetadataSet(reference);
        metadata.put("dummy", Arrays.asList("dummy"));

        return addMetadataToSampleImage(reference, metadata);
    }
}