package es.uji.apps.ade.services.rest.transforms.scale;

import com.sun.jersey.api.client.ClientResponse;
import es.uji.apps.ade.core.media.RawImage;
import es.uji.apps.ade.services.rest.AbstractRestResourceTest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class ScaleAbstractRestResourceTest extends AbstractRestResourceTest
{
    protected static final int NEW_WIDTH_SCALE = 200;
    protected static final int NEW_HEIGHT_SCALE = 200;

    protected RawImage callDefaultScaleResource(String reference) throws IOException
    {
        ClientResponse response = resource.path("/storage/" + reference)
                .queryParam("t", "SCALE")
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        InputStream dataStream = response.getEntity(InputStream.class);

        return new RawImage(dataStream);
    }

    protected RawImage callScaleResource(String reference, int newWidth, String dimension) throws IOException
    {
        ClientResponse response = resource.path("/storage/" + reference)
                .queryParam("t", "SCALE," + dimension + "=" + newWidth)
                .header("X-UJI-AuthToken", authToken)
                .get(ClientResponse.class);

        InputStream dataStream = response.getEntity(InputStream.class);

        return new RawImage(dataStream);
    }

    protected RawImage retrieveScaledCachedImageFromStorage(String reference) throws IOException
    {
        Path path = Paths.get(STORAGE_PATH, reference.substring(0, 2), reference.substring(2, 4), reference + ".S");
        return new RawImage(Files.newInputStream(path));
    }
}
