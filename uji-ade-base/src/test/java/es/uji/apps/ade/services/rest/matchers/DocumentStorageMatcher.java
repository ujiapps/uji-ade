package es.uji.apps.ade.services.rest.matchers;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DocumentStorageMatcher extends TypeSafeMatcher<String>
{
    private String storagePath;

    public DocumentStorageMatcher(String storagePath)
    {
        this.storagePath = storagePath;
    }

    @Override
    public boolean matchesSafely(String reference)
    {
        Path path = Paths.get(storagePath, reference.substring(0, 2), reference.substring(2, 4), reference);
        return Files.exists(path);
    }

    public void describeTo(Description description)
    {
        description.appendText("doen't exist in the " + storagePath + " storage");
    }

    @Factory
    public static <T> Matcher<String> storedAt(String storagePath)
    {
        return new DocumentStorageMatcher(storagePath);
    }
}