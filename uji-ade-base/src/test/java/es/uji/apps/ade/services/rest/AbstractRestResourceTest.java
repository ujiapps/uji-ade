package es.uji.apps.ade.services.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import es.uji.apps.ade.core.dao.DatabaseDocumentDAO;
import es.uji.apps.ade.core.storage.DocumentPersister;
import es.uji.commons.rest.json.UIEntityListMessageBodyReader;
import es.uji.commons.rest.json.UIEntityMessageBodyReader;
import es.uji.commons.rest.json.UIEntityMessageBodyWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.util.Log4jConfigListener;

import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class AbstractRestResourceTest extends JerseyTest
{
    protected static String packageName = "es.uji.apps.ade.services.rest";
    protected static final String STORAGE_PATH = "/mnt/dbobjects/alma";

    protected String authToken = "a025ac8e23131b038a0b7f949faeb04d382e5b25";

    protected WebResource resource;

    @Autowired
    protected DatabaseDocumentDAO databaseDocumentDAO;

    @Autowired
    protected DocumentPersister documentPersister;

    public AbstractRestResourceTest()
    {
        super(new WebAppDescriptor.Builder(packageName)
                .contextParam("contextConfigLocation", "classpath:applicationContext-test.xml")
                .contextParam("webAppRootKey", packageName)
                .contextListenerClass(Log4jConfigListener.class)
                .contextListenerClass(ContextLoaderListener.class)
                .requestListenerClass(RequestContextListener.class)
                .servletClass(SpringServlet.class)
                .clientConfig(createClientConfig())
                .initParam("com.sun.jersey.config.property.packages",
                        "es.uji.commons.rest.shared; es.uji.commons.rest.json; " + packageName).build());

        this.resource = resource();
    }

    private static ClientConfig createClientConfig()
    {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(UIEntityMessageBodyReader.class);
        config.getClasses().add(UIEntityMessageBodyWriter.class);
        config.getClasses().add(UIEntityListMessageBodyReader.class);

        return config;
    }

    protected FormDataMultiPart buildSampleDocument() throws Exception
    {
        return buildSampleDocument(null);
    }

    protected FormDataMultiPart buildSampleDocument(String reference) throws Exception
    {
        FormDataMultiPart responseMultipart = new FormDataMultiPart();
        Path path = Paths.get("src/test/resources/prueba.jpg");

        responseMultipart.field("name", "prueba.jpg");
        responseMultipart.field("mimetype", "image/jpeg");

        if (reference != null)
        {
            responseMultipart.field("reference", reference);
        }

        responseMultipart.field("contents", Files.readAllBytes(path), MediaType.valueOf("image/jpeg"));

        return responseMultipart;
    }

    protected ClientResponse addDocument(FormDataMultiPart data) throws Exception
    {
        return buildResource().type(MediaType.MULTIPART_FORM_DATA)
                .post(ClientResponse.class, data);
    }

    protected ClientResponse updateWatermark(String reference, String waterMark) throws Exception
    {
        MultivaluedMap form = new MultivaluedMapImpl();
        form.add("watermark", waterMark);

        return resource.path("/storage/" + reference + "/watermark")
                .header("X-UJI-AuthToken", authToken)
                .type(MediaType.APPLICATION_FORM_URLENCODED)
                .put(ClientResponse.class, form);
    }

    protected ClientResponse deleteDocumentWithRef(String reference)
    {
        return buildResource(reference).delete(ClientResponse.class);
    }

    protected ClientResponse retrieveDocumentWithRef(String reference)
    {
        return buildResource(reference).get(ClientResponse.class);
    }

    protected WebResource.Builder buildResource(String reference)
    {
        return resource.path("/storage/" + reference).header("X-UJI-AuthToken", authToken);
    }

    protected WebResource.Builder buildResource()
    {
        return resource.path("/storage/").header("X-UJI-AuthToken", authToken);
    }

    protected String createSampleImage() throws Exception
    {
        ClientResponse response = addDocument(buildSampleDocument());
        JsonNode responseMessage = response.getEntity(JsonNode.class);
        JsonNode payload = responseMessage.path("data");

        return payload.path("reference").asText();
    }

    public String createAndStoreSampleImage() throws Exception
    {
        String reference = createSampleImage();

        documentPersister.savePendingDocumentsToStorage();

        return reference;
    }
}
