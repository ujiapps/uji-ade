package es.uji.apps.ade.services.rest;

import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.db.DocumentMetadataDTO;
import es.uji.apps.ade.core.services.MetadataService;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

@Path("metadata")
@Service
public class MetadataResource
{
    @Autowired
    private MetadataService metadataService;

    @PUT
    @Path("{reference}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String, List<String>> overwriteMetadata(@PathParam("reference") String reference, MultivaluedMap<String, String> params)
            throws StorageIOException
    {
        Map<String, List<String>> inputParams = (Map) params;

        metadataService.overwriteMetadata(reference, inputParams);

        return inputParams;
    }

    @GET
    @Path("{reference}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getMetadata(@PathParam("reference") String reference) throws StorageIOException, IOException
    {
        return metadataService.retrieveMetadata(reference);
    }
}