package es.uji.apps.ade.auth.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class NonGetRequestsWithTokenAuth implements Filter
{
    private FilterConfig filterConfig;

    public void init(FilterConfig filterConfig) throws ServletException
    {
        this.filterConfig = filterConfig;
    }

    public void destroy()
    {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        HttpServletRequest clientRequest = (HttpServletRequest) request;

        String requestVerb = clientRequest.getMethod();

        if (requestVerb == null || requestVerb.equalsIgnoreCase("GET"))
        {
            chain.doFilter(request, response);
            return;
        }

        String headerAuthToken = clientRequest.getHeader("X-UJI-AuthToken");

        if (headerAuthToken != null)
        {
            String token = filterConfig.getInitParameter("authToken");

            if (token != null && headerAuthToken.equals(token))
            {
                chain.doFilter(request, response);
                return;
            }
        }

        throw new ServletException("Forbidden");
    }
}
