package es.uji.apps.ade.services.rest;

import es.uji.apps.ade.core.model.Document;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.http.util.EncodingUtils;

import javax.print.Doc;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZippedDocumentsStream implements StreamingOutput
{
    private List<Document> documents;

    public ZippedDocumentsStream(List<Document> documents)
    {
        this.documents = documents;
    }

    public void write(OutputStream output) throws IOException, WebApplicationException
    {
        ZipOutputStream zipOut = new ZipOutputStream(output);
        ZipEntry zipEntry;
        int read = 0;
        byte[] bytes = new byte[1024];

        for (Document document: this.documents) {
            zipEntry = new ZipEntry(document.getName());
            zipOut.putNextEntry(zipEntry);
            InputStream content = document.getContents();

            while ((read = content.read(bytes)) != -1)
            {
                zipOut.write(bytes, 0, read);
            }

            content.close();
        }

        zipOut.flush();
        zipOut.close();
        output.flush();
        output.close();
    }
}