package es.uji.apps.ade.services.rest;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.ade.core.exceptions.DocumentNotFoundException;
import es.uji.apps.ade.core.exceptions.DocumentReferencesNotFoundInStorageException;
import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.db.DocumentDTO;
import es.uji.apps.ade.core.model.domains.TransformType;
import es.uji.apps.ade.core.model.transforms.TransformList;
import es.uji.apps.ade.core.model.transforms.TransformParam;
import es.uji.apps.ade.core.services.StorageService;
import es.uji.commons.rest.StreamUtils;
import es.uji.commons.rest.UIEntity;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Path("storage")
@Service
public class StorageResource {
    @InjectParam
    private StorageService storageService;

    @GET
    @Path("{reference}")
    public Response get(@PathParam("reference") String reference, @QueryParam("cd") String contentDisposition,
                        @QueryParam("t") List<String> transformsParams)
            throws DocumentNotFoundException, StorageIOException, InvalidTransformParameterException {
        TransformList transforms = buildTransformList(transformsParams);

        Document document = storageService.retrieveDocument(reference, transforms);
        DocumentStream documentStream = new DocumentStream(document);

        String currentContentDisposition = "inline";

        if (isValidContentDisposition(contentDisposition)) {
            currentContentDisposition = contentDisposition;
        }

        return Response.ok(documentStream)
                .type(document.getMimeType())
                .header("Content-Disposition", currentContentDisposition + "; filename=\"" + document.getName() + "\"")
                .build();
    }

    @PUT
    @Path("{reference}/watermark")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void updateWatermark(@PathParam("reference") String reference, @FormParam("watermark") String waterMark) throws IOException {
        storageService.updateWatermark(reference, waterMark);
    }

    @GET
    @Path("export/{scope}/{hash}")
    public Response get(@PathParam("scope") String scope, @PathParam("hash") String hash)
            throws StorageIOException, DocumentNotFoundException, InvalidTransformParameterException,
            DocumentReferencesNotFoundInStorageException {
        List<Document> documentsToZip = storageService.retriveDocumentsToExport(scope, hash);

        ZippedDocumentsStream documentStream = new ZippedDocumentsStream(documentsToZip);

        return Response.ok(documentStream)
                .type("application/zip")
                .header("Content-Disposition", "attachment; filename=" + scope + "_" + hash + ".zip")
                .build();
    }

    private boolean isValidContentDisposition(String contentDisposition) {
        return (contentDisposition != null && (contentDisposition.toLowerCase()
                .equals("inline") || contentDisposition.toLowerCase().equals("attachment")));
    }

    private TransformList buildTransformList(List<String> transformsParams) {
        TransformList transforms = new TransformList();

        for (String transform : transformsParams) {
            List<String> paramList = Arrays.asList(transform.split(","));
            TransformType type = TransformType.valueOf(paramList.get(0));

            List<String> params = paramList.subList(1, paramList.size());

            List<TransformParam> transformParams = params.stream().map(param -> {
                String key = param.split("=")[0];
                String value = param.split("=")[1];

                return new TransformParam(key, value);
            }).collect(toList());

            transforms.add(type, transformParams);
        }

        return transforms;
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity post(FormDataMultiPart multiPart)
            throws IOException {
        checkContentIsNotNull(multiPart);

        String reference = getParamString(multiPart, "reference");
        String nombre = getParamString(multiPart, "name");
        String mimeType = getParamString(multiPart, "mimetype");

        InputStream contentStream = multiPart.getField("contents").getEntityAs(InputStream.class);
        byte[] contents = StreamUtils.inputStreamToByteArray(contentStream);

        DocumentDTO document = storageService.storeDocument(nombre, reference, mimeType, contents);

        return UIEntity.toUI(document);
    }

    private void checkContentIsNotNull(FormDataMultiPart multiPart) {
        if (multiPart.getField("contents") == null) {
            throw new IllegalArgumentException();
        }
    }

    private String getParamString(FormDataMultiPart multiPart, String param) {
        if (multiPart != null && multiPart.getField(param) != null) {
            return multiPart.getField(param).getValue();
        }

        return null;
    }

    @DELETE
    @Path("{reference}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("reference") String reference)
            throws StorageIOException {
        storageService.markDocumentAsPendingToDelete(reference);
        return Response.noContent().build();
    }
}
