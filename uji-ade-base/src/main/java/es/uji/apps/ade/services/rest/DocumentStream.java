package es.uji.apps.ade.services.rest;

import es.uji.apps.ade.core.model.Document;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DocumentStream implements StreamingOutput
{
    private Document document;

    public DocumentStream(Document document)
    {
        this.document = document;
    }

    public void write(OutputStream output) throws IOException, WebApplicationException
    {
        InputStream contents = document.getContents();

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = contents.read(bytes)) != -1)
        {
            output.write(bytes, 0, read);
        }

        output.flush();
        output.close();
        contents.close();
    }
}