package es.uji.apps.ade.core.fulltext;

import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.RecursiveParserWrapper;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BasicContentHandlerFactory;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class TextExtractor
{
    enum Language
    {
        ENGLISH("eng"), SPANISH("spa"), CATALAN("cat");

        private String language;

        Language(String language)
        {
            this.language = language;
        }

        @Override
        public String toString()
        {
            return this.language;
        }
    }

    private static final Parser PARSERS[] = new Parser[]{
            new org.apache.tika.parser.html.HtmlParser(),
            new org.apache.tika.parser.rtf.RTFParser(),
            new org.apache.tika.parser.pdf.PDFParser(),
            new org.apache.tika.parser.txt.TXTParser(),
            new org.apache.tika.parser.microsoft.OfficeParser(),
            new org.apache.tika.parser.microsoft.OldExcelParser(),
            new org.apache.tika.parser.microsoft.ooxml.OOXMLParser(),
            new org.apache.tika.parser.odf.OpenDocumentParser(),
            new org.apache.tika.parser.iwork.IWorkPackageParser(),
            new org.apache.tika.parser.xml.DcXMLParser()
    };

    private static final AutoDetectParser PARSER_INSTANCE = new AutoDetectParser(PARSERS);

    private static final Tika TIKA_INSTANCE = new Tika(PARSER_INSTANCE.getDetector(), PARSER_INSTANCE);

    static String extract(byte content[]) throws TikaException, IOException
    {
        return TIKA_INSTANCE.parseToString(new ByteArrayInputStream(content));
    }

    static String ocr(Language language, byte content[]) throws TikaException, SAXException, IOException
    {
        TesseractOCRConfig config = new TesseractOCRConfig();
        config.setLanguage(language.toString());

        Parser parser = new RecursiveParserWrapper(new AutoDetectParser(),
                new BasicContentHandlerFactory(BasicContentHandlerFactory.HANDLER_TYPE.TEXT, -1));

        PDFParserConfig pdfConfig = new PDFParserConfig();
        pdfConfig.setExtractInlineImages(true);

        ParseContext parseContext = new ParseContext();
        parseContext.set(TesseractOCRConfig.class, config);
        parseContext.set(Parser.class, parser);
        parseContext.set(PDFParserConfig.class, pdfConfig);

        parser.parse(new ByteArrayInputStream(content), null, new Metadata(), parseContext);

        List<Metadata> metadataList = ((RecursiveParserWrapper) parser).getMetadata();

        return metadataList.stream()
                .map(m -> m.get(RecursiveParserWrapper.TIKA_CONTENT))
                .collect(joining(""));
    }
}