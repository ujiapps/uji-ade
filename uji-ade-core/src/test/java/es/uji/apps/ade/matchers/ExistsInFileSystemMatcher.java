package es.uji.apps.ade.matchers;

import es.uji.apps.ade.FileSystem;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.File;

public class ExistsInFileSystemMatcher extends TypeSafeMatcher<String>
{
    @Override
    public boolean matchesSafely(String filename)
    {
        String level1 = filename.substring(0, 2);
        String level2 = filename.substring(2, 4);

        return new File(FileSystem.STORAGE_PATH + "/" + level1 + "/" + level2 + "/" + filename).exists();
    }

    public void describeTo(Description description)
    {
        description.appendText("New file not stored in filesystem");
    }

    @Factory
    public static <T> Matcher<String> existsInFileSystem()
    {
        return new ExistsInFileSystemMatcher();
    }
}