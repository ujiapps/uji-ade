package es.uji.apps.ade;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ProjectConfig
{
    private final Properties props;

    public ProjectConfig() throws IOException
    {
        props = new Properties();
        props.load(new FileInputStream("/etc/uji/ade/app.properties"));
    }

    public String getFulltextHost()
    {
        return props.getProperty("uji.fulltext.host");
    }

    public String getFulltextPort()
    {
        return props.getProperty("uji.fulltext.port");
    }

    public String getFulltextIndex()
    {
        return props.getProperty("uji.fulltext.index");
    }

    public String getFulltextType()
    {
        return props.getProperty("uji.fulltext.type");
    }
}

