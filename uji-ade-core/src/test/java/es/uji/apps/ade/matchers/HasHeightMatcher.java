package es.uji.apps.ade.matchers;

import es.uji.apps.ade.core.media.RawImage;
import es.uji.apps.ade.core.model.Document;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.ByteArrayInputStream;

public class HasHeightMatcher extends TypeSafeMatcher<Document>
{
    private int expectedHeight;
    private int thumbnailHeight;

    public HasHeightMatcher(int expectedHeight)
    {
        this.expectedHeight = expectedHeight;
    }

    @Override
    public boolean matchesSafely(Document document)
    {
        try
        {
            RawImage image = new RawImage(document.getContents());
            this.thumbnailHeight = image.getHeight();

            return this.thumbnailHeight == this.expectedHeight;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void describeTo(Description description)
    {
        description.appendText("height " + this.thumbnailHeight + " doesn't match expected " + expectedHeight);
    }

    @Factory
    public static <T> Matcher<Document> hasHeight(int expectedHeight)
    {
        return new HasHeightMatcher(expectedHeight);
    }
}