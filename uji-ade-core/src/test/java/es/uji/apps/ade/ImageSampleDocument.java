package es.uji.apps.ade;

import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.ImageDocument;
import es.uji.commons.rest.StreamUtils;
import org.springframework.http.MediaType;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

public class ImageSampleDocument extends ImageDocument
{
    public ImageSampleDocument() throws IOException
    {
        super(UUID.randomUUID().toString());

        String path = "src/test/resources/prueba.jpg";

        setName("prueba.jpg");
        setMimeType(MediaType.IMAGE_JPEG_VALUE);
        setContents(new FileInputStream(path));
    }
}
