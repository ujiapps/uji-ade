package es.uji.apps.ade.core.fulltext;

import es.uji.apps.ade.ProjectConfig;
import es.uji.apps.ade.core.fulltext.beans.SampleBean;
import es.uji.commons.rest.StreamUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.UUID;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

public class GeneralBeanIndexAndSearchTest
{
    private ElasticSearchClient client;

    private SampleBean bean;

    @Before
    public void init() throws Exception
    {
        bean = new SampleBean();
        bean.setField1("campo 1");
        bean.setField2("campo 2");

        ProjectConfig config = new ProjectConfig();

        client = new ElasticSearchClient(config.getFulltextHost(), config.getFulltextPort(), "test", "spike");
        client.dropIndex();
        client.createIndex();
    }

    @Test
    public void shouldIndexNewDocumentsWithoutInternalId() throws Exception
    {
        String uuid = client.addSync(bean);
        assertThat(uuid, is(notNullValue()));

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", bean.getField1());
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }

    @Test
    public void shouldIndexNewDocumentsWithInternalId() throws Exception
    {
        String uuid = UUID.randomUUID().toString();

        client.addSync(uuid, bean);

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", bean.getField1());
        assertThat(search.getTotal(), is(greaterThan(0L)));

        SampleBean singleResult = client.get(SampleBean.class, uuid);
        assertThat(singleResult.getField1(), is("campo 1"));
        assertThat(singleResult.getField2(), is("campo 2"));
    }

    @Test
    public void shouldUpdateAnAlreadyIndexedDocument() throws Exception
    {
        String uuid = client.addSync(bean);

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", "campo 1");
        assertThat(search.getTotal(), is(greaterThan(0L)));

        bean.setField1("campo 11");
        client.addSync(uuid, bean);

        search = client.search(SampleBean.class, "field1", "campo 11");
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }
}
