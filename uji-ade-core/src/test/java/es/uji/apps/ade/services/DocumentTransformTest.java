package es.uji.apps.ade.services;

import es.uji.apps.ade.FileSystem;
import es.uji.apps.ade.ImageSampleDocument;
import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.media.RawImage;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.ImageDocument;
import es.uji.apps.ade.core.model.domains.StorageResult;
import es.uji.apps.ade.core.model.domains.TransformType;
import es.uji.apps.ade.core.model.transforms.ScaleTransform;
import es.uji.apps.ade.core.model.transforms.TransformList;
import es.uji.apps.ade.core.model.transforms.TransformParam;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.apps.ade.core.storage.filesystem.FilesystemStorage;
import es.uji.commons.rest.StreamUtils;
import org.junit.Test;
import org.springframework.http.MediaType;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.UUID;

import static es.uji.apps.ade.matchers.HasHeightMatcher.hasHeight;
import static es.uji.apps.ade.matchers.HasWidthMatcher.hasWidth;
import static org.hamcrest.MatcherAssert.assertThat;

public class DocumentTransformTest
{

    public static final String TRANSFORM_WIDTH = "200";
    public static final String TRANSFORM_HEIGHT = "200";
    public static final String TRANSFORM_DEGREES = "-90";

    @Test
    public void shouldGenerateScaledWidthPreview() throws Exception
    {
        TransformParam width = new TransformParam("w", TRANSFORM_WIDTH);
        TransformList transforms = new TransformList();
        transforms.add(TransformType.SCALE, Collections.singletonList(width));

        Document thumbnail = transformThumbnail(transforms);

        assertThat(thumbnail, hasWidth(Integer.parseInt(TRANSFORM_WIDTH)));
    }

    @Test
    public void shouldGenerateScaledHeightPreview() throws Exception
    {
        TransformParam height = new TransformParam("h", TRANSFORM_HEIGHT);
        TransformList transforms = new TransformList();
        transforms.add(TransformType.SCALE, Collections.singletonList(height));

        Document thumbnail = transformThumbnail(transforms);

        assertThat(thumbnail, hasHeight(Integer.parseInt(TRANSFORM_HEIGHT)));
    }

    @Test
    public void shouldGenerateRotatedPreview() throws Exception
    {
        TransformParam degrees = new TransformParam("d", TRANSFORM_DEGREES);
        TransformList transforms = new TransformList();
        transforms.add(TransformType.ROTATE, Collections.singletonList(degrees));

        RawImage originalImage = new RawImage(new FileInputStream("src/test/resources/prueba.jpg"));
        Document thumbnail = transformThumbnail(transforms);

        assertThat(thumbnail, hasWidth(originalImage.getHeight()));
        assertThat(thumbnail, hasHeight(originalImage.getWidth()));
    }

    private Document transformThumbnail(TransformList transforms)
            throws IOException, StorageIOException, StorageUnavailableException, InvalidTransformParameterException
    {
        Document image = new ImageSampleDocument();
        Storage storage = new FilesystemStorage(FileSystem.STORAGE_PATH);

        StorageResult result = storage.add(image);
        image.setUid(result.uuid);

        return transforms.apply(image);
    }
}