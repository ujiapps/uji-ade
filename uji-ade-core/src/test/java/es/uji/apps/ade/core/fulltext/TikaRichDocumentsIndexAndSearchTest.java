package es.uji.apps.ade.core.fulltext;

import es.uji.apps.ade.ProjectConfig;
import es.uji.apps.ade.core.fulltext.beans.SampleBean;
import es.uji.apps.ade.core.fulltext.beans.SampleRichBean;
import es.uji.commons.rest.StreamUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class TikaRichDocumentsIndexAndSearchTest
{
    private static final String EXTRACTED_TEXT = "Hola esto es una una prueba de indexación en documentos pdf";

    private ElasticSearchClient client;

    private SampleBean bean;

    @Before
    public void init() throws Exception
    {
        byte[] content = StreamUtils.inputStreamToByteArray(new FileInputStream("src/test/resources/test.pdf"));
        String extractedTexT = TextExtractor.extract(content);

        System.out.println(extractedTexT);

        bean = new SampleBean();
        bean.setField1("campo 1");
        bean.setField2(extractedTexT);

        ProjectConfig config = new ProjectConfig();

        client = new ElasticSearchClient(config.getFulltextHost(), config.getFulltextPort(), "test", "spike");
        client.dropIndex();
        client.createIndex();
    }

    @Test
    public void shouldIndexNewDocumentsWithoutInternalId() throws Exception
    {
        String uuid = client.addSync(bean);
        assertThat(uuid, is(notNullValue()));

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", bean.getField1());
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }

    @Test
    public void shouldIndexNewDocumentsWithInternalId() throws Exception
    {
        String uuid = UUID.randomUUID().toString();

        client.addSync(uuid, bean);

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", bean.getField1());
        assertThat(search.getTotal(), is(greaterThan(0L)));

        SampleBean singleResult = client.get(SampleBean.class, uuid);
        assertThat(singleResult.getField1(), is("campo 1"));
        assertThat(singleResult.getField2().trim(), containsString("Hola"));
    }

    @Test
    public void shouldUpdateAnAlreadyIndexedDocument() throws Exception
    {
        String uuid = client.addSync(bean);

        SearchResponse<SampleBean> search = client.search(SampleBean.class, "field1", "campo 1");
        assertThat(search.getTotal(), is(greaterThan(0L)));

        bean.setField1("campo 11");
        client.addSync(uuid, bean);

        search = client.search(SampleBean.class, "field1", "campo 11");
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }
}
