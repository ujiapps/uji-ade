package es.uji.apps.ade.matchers;

import es.uji.apps.ade.core.media.RawImage;
import es.uji.apps.ade.core.model.Document;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.io.ByteArrayInputStream;

public class HasWidthMatcher extends TypeSafeMatcher<Document>
{
    private int expectedWidth;
    private int thumbnailWith;

    public HasWidthMatcher(int expectedWidth)
    {
        this.expectedWidth = expectedWidth;
    }

    @Override
    public boolean matchesSafely(Document document)
    {
        try
        {
            RawImage image = new RawImage(document.getContents());
            this.thumbnailWith = image.getWidth();

            return this.thumbnailWith == this.expectedWidth;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void describeTo(Description description)
    {
        description.appendText("width " + this.thumbnailWith + " doesn't match expected " + expectedWidth);
    }

    @Factory
    public static <T> Matcher<Document> hasWidth(int expectedWidth)
    {
        return new HasWidthMatcher(expectedWidth);
    }
}