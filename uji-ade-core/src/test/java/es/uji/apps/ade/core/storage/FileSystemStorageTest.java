package es.uji.apps.ade.core.storage;

import es.uji.apps.ade.FileSystem;
import es.uji.apps.ade.ImageSampleDocument;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.storage.filesystem.FilesystemStorage;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static es.uji.apps.ade.matchers.ExistsInFileSystemMatcher.existsInFileSystem;
import static org.hamcrest.MatcherAssert.assertThat;

public class FileSystemStorageTest
{
    private Document image;

    @Before
    public void init() throws IOException
    {
        image = new ImageSampleDocument();
    }

    @Test
    public void shouldStoreNewDocuments() throws Exception
    {
        Storage storage = new FilesystemStorage(FileSystem.STORAGE_PATH);

        storage.add(image);

        assertThat(image.getReference(), existsInFileSystem());
    }
}