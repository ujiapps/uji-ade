package es.uji.apps.ade.core.fulltext;

import es.uji.apps.ade.ProjectConfig;
import es.uji.apps.ade.core.fulltext.beans.SampleRichBean;
import es.uji.commons.rest.StreamUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.Arrays;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class RichDocumentsIndexAndSearchTest
{
    private ElasticSearchClient client;

    private SampleRichBean richBean;

    @Before
    public void init() throws Exception
    {
        richBean = new SampleRichBean();
        richBean.setField1("campo 1");
        richBean.setField2("campo 2");
        richBean.setBinary(StreamUtils.inputStreamToByteArray(new FileInputStream("src/test/resources/test.pdf")));

        ProjectConfig config = new ProjectConfig();

        client = new ElasticSearchClient(config.getFulltextHost(), config.getFulltextPort(), "test", "spike");
        client.dropIndex();
        client.createIndex();
        client.enableRichContentFor("binary");
    }

    @Test
    public void shouldIndexAndSearchPDFFiles() throws Exception
    {
        client.addSync(richBean);

        SearchResponse<SampleRichBean> search = client.search(SampleRichBean.class, "binary.content", "indexación");
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }

    @Test
    public void shouldIndexAndSearchPDFFilesIgnoringDiacriticalMarks() throws Exception
    {
        client.addSync(richBean);

        SearchResponse<SampleRichBean> search = client.search(SampleRichBean.class, "binary.content", "indexacion");
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }

    @Test
    public void shouldIndexAndSearchPDFFilesIgnoringCase() throws Exception
    {
        client.addSync(richBean);

        SearchResponse<SampleRichBean> search = client.search(SampleRichBean.class, "binary.content", "PRUEBA");
        assertThat(search.getTotal(), is(greaterThan(0L)));
    }

    @Test
    public void shouldIndexPDFFilesAndReturnOnlyCertainColumns() throws Exception
    {
        client.addSync(richBean);

        SearchResponse<SampleRichBean> search = client.search(SampleRichBean.class,
                "binary.content", "prueba",
                Arrays.asList("field1", "field2"));
        assertThat(search.getTotal(), is(greaterThan(0L)));

        SampleRichBean indexedRecord = search.getRecords().get(0);
        assertThat(indexedRecord.getField1(), is(notNullValue()));
        assertThat(indexedRecord.getField2(), is(notNullValue()));
        assertThat(indexedRecord.getBinary(), is(nullValue()));
    }
}
