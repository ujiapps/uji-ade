package es.uji.apps.ade.core.fulltext;

import es.uji.apps.ade.ProjectConfig;
import es.uji.apps.ade.core.fulltext.beans.SampleBean;
import es.uji.commons.rest.StreamUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.UUID;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class OcrIndexAndSearchTest
{
    private ElasticSearchClient client;

    private SampleBean bean;

    @Before
    public void init() throws Exception
    {
        ProjectConfig projectConfig = new ProjectConfig();

        client = new ElasticSearchClient(projectConfig.getFulltextHost(), projectConfig.getFulltextPort(), "test", "spike");
        client.dropIndex();
        client.createIndex();
    }

    @Test
    public void shouldApplyOcrToPNGFiles() throws Exception
    {
        byte[] content = StreamUtils.inputStreamToByteArray(new FileInputStream("src/test/resources/ocr-image-spa-001.png"));
        String text = TextExtractor.ocr(TextExtractor.Language.SPANISH, content);

        bean = new SampleBean();
        bean.setField1("campo 1");
        bean.setField2(text);

        String uuid = UUID.randomUUID().toString();

        client.addSync(uuid, bean);

        SampleBean singleResult = client.get(SampleBean.class, uuid);
        assertThat(singleResult.getField1(), is("campo 1"));
        assertThat(singleResult.getField2().trim(), containsString("Hola"));
    }

    @Test
    public void shouldApplyOcrToPDFFiles() throws Exception
    {
        byte[] content = StreamUtils.inputStreamToByteArray(new FileInputStream("src/test/resources/ocr-pdf-spa-001.pdf"));
        String text = TextExtractor.ocr(TextExtractor.Language.SPANISH, content);

        bean = new SampleBean();
        bean.setField1("campo 1");
        bean.setField2(text);

        String uuid = UUID.randomUUID().toString();

        client.addSync(uuid, bean);

        SampleBean singleResult = client.get(SampleBean.class, uuid);
        assertThat(singleResult.getField1(), is("campo 1"));
        assertThat(singleResult.getField2().trim(), containsString("Hola"));
    }

    @Test
    public void shouldApplyOcrToJPGFiles() throws Exception
    {
        byte[] content = StreamUtils.inputStreamToByteArray(new FileInputStream("src/test/resources/ocr-image-spa-001.jpg"));
        String text = TextExtractor.ocr(TextExtractor.Language.SPANISH, content);

        bean = new SampleBean();
        bean.setField1("campo 1");
        bean.setField2(text);

        String uuid = UUID.randomUUID().toString();

        client.addSync(uuid, bean);

        SampleBean singleResult = client.get(SampleBean.class, uuid);
        assertThat(singleResult.getField1(), is("campo 1"));
        assertThat(singleResult.getField2().trim(), containsString("José Luis Blasco Diaz"));
    }
}