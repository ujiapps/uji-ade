package es.uji.apps.ade.core.model.transforms;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;

import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.ImageDocument;

public class ScaleTransform extends Transform
{
    public static final Integer PREVIEW_WIDTH_SIZE = 630;
    public static final Integer PREVIEW_HEIGHT_SIZE = 354;

    private static final Integer PROPORTIONAL_ASPECT_RATIO_VALUE = -1;

    private static final String WIDTH_PARAM = "w";
    private static final String HEIGHT_PARAM = "h";

    public ScaleTransform(List<TransformParam> params)
    {
        super(params);
    }

    @Override
    public Document apply(Document document) throws StorageIOException, InvalidTransformParameterException
    {
        if (notApplies(document))
        {
            return document;
        }

        Document scaledDocument = document.clone();
        scaledDocument.setContents(new ByteArrayInputStream(generateScaledContent(document.getContents())));

        return scaledDocument;
    }

    private boolean notApplies(Document document)
    {
        return !(document instanceof ImageDocument || document.getMimeType().startsWith("image/"));
    }

    private byte[] generateScaledContent(InputStream contents) throws StorageIOException, InvalidTransformParameterException
    {
        Integer width = getDesiredWidth();
        Integer height = getDesiredHeight();

        try
        {
            BufferedImage sourceImage = ImageIO.read(contents);
            Image thumbnail = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
                    thumbnail.getHeight(null), BufferedImage.TYPE_INT_RGB);
            bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedThumbnail, "jpg", baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            throw new StorageIOException();
        }
    }

    private Integer getDesiredWidth() throws InvalidTransformParameterException
    {
        if (params.size() != 1)
        {
            return PREVIEW_WIDTH_SIZE;
        }

        if (hasParam(WIDTH_PARAM))
        {
            TransformParam param = getParam(WIDTH_PARAM);
            return param.getIntegerValue();
        }

        return PROPORTIONAL_ASPECT_RATIO_VALUE;
    }

    private Integer getDesiredHeight() throws InvalidTransformParameterException
    {
        if (params.size() != 1)
        {
            return PROPORTIONAL_ASPECT_RATIO_VALUE;
        }

        if (hasParam(HEIGHT_PARAM))
        {
            TransformParam param = getParam(HEIGHT_PARAM);
            return param.getIntegerValue();
        }

        return PROPORTIONAL_ASPECT_RATIO_VALUE;
    }
}