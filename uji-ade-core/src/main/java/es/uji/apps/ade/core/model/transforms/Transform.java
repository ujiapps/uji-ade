package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;

import java.io.IOException;
import java.util.List;

public abstract class Transform
{
    protected List<TransformParam> params;

    public Transform(List<TransformParam> params)
    {
        this.params = params;
    }

    public abstract Document apply(Document document) throws StorageIOException, InvalidTransformParameterException;

    public TransformParam getParam(String name)
    {
        for (TransformParam param : params)
        {
            if (param.getName().equals(name))
            {
                return param;
            }
        }

        return null;
    }

    public boolean hasParam(String name)
    {
        return getParam(name) != null;
    }
}
