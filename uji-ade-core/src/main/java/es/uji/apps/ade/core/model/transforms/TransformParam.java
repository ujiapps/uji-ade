package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;

public class TransformParam
{
    private String name;
    private String value;

    public TransformParam(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public Float getFloatValue() throws InvalidTransformParameterException
    {
        try
        {
            return Float.parseFloat(value);
        }
        catch (NumberFormatException e)
        {
            throw new InvalidTransformParameterException();
        }
    }

    public Integer getIntegerValue() throws InvalidTransformParameterException
    {
        try
        {
            return Integer.valueOf(value);
        }
        catch (NumberFormatException e)
        {
            throw new InvalidTransformParameterException();
        }

    }
}
