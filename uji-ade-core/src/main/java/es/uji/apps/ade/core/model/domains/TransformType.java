package es.uji.apps.ade.core.model.domains;

import es.uji.apps.ade.core.model.transforms.*;

import java.util.List;

public enum TransformType
{
    SCALE("S")
            {
                public Transform build(List<TransformParam> params)
                {
                    return new ScaleTransform(params);
                }
            },
    ROTATE("R")
            {
                public Transform build(List<TransformParam> params)
                {
                    return new RotateTransform(params);
                }
            },
    WATERMARK("W")
            {
                public Transform build(List<TransformParam> params)
                {
                    return new WatermarkTransform(params);
                }
            },
    GREY("Y"),
    NULL("N");

    private String id;

    public Transform build(List<TransformParam> params)
    {
        return new NullTransform(params);
    }



    TransformType(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
}
