package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class DocumentNotFoundException extends Exception
{
    public DocumentNotFoundException()
    {
        super("The requested document doesn't exists");
    }
}
