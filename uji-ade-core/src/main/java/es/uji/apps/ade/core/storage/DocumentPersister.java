package es.uji.apps.ade.core.storage;

import es.uji.apps.ade.core.dao.DatabaseDocumentDAO;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.db.DocumentDTO;
import es.uji.apps.ade.core.model.db.DocumentMetadataDTO;
import es.uji.apps.ade.core.model.domains.StorageResult;
import es.uji.apps.ade.core.services.SignatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;

@Component
public class DocumentPersister
{
    private static Logger log = LoggerFactory.getLogger(DocumentPersister.class);

    private DatabaseDocumentDAO databaseDocumentDAO;
    private IndexationClient indexationClient;
    private SignatureService signatureService;

    @Autowired
    public DocumentPersister(DatabaseDocumentDAO databaseDocumentDAO, IndexationClient indexationClient, SignatureService signatureService)
    {
        this.databaseDocumentDAO = databaseDocumentDAO;
        this.indexationClient = indexationClient;
        this.signatureService = signatureService;
    }

    public void removeExpiredDocuments()
    {
        for (DocumentDTO documentDTO : databaseDocumentDAO.getPendingFilesToRemove())
        {
            Storage storage = StorageFactory.build(documentDTO.getStorageTypeId());

            try
            {
                String uid = documentDTO.getStorageUID();

                log.info("Deleting Document " + uid);
                storage.remove(documentDTO.getStorageUID());
                databaseDocumentDAO.setDocumentAsDeleted(documentDTO.getId());

                indexationClient.delete(documentDTO.getStorageUID());

                log.info("File " + documentDTO.getFileName() + " marked as removed");
            }
            catch (Exception e)
            {
                log.error("Error deleting document: ", e);
            }
        }
    }

    public void savePendingDocumentsToStorage()
    {
        for (Long documentId : databaseDocumentDAO.getPendingIdsToMigrate())
        {
            DocumentDTO documentDTO = databaseDocumentDAO.getPendingFileToMigrateById(documentId);
            Storage storage = StorageFactory.build(documentDTO.getStorageTypeId());

            try
            {
                log.info("Migrating file " + documentDTO.getFileName());

                if (documentDTO.isPDF())
                {
                    byte[] signedData = signatureService.signPdf(documentDTO.getFileContents());
                    documentDTO.setFileContents(signedData);
                }

                Document document = new Document(documentDTO.getReference());
                document.setName(documentDTO.getFileName());
                document.setMimeType(documentDTO.getMimeType());
                document.setContents(new ByteArrayInputStream(documentDTO.getFileContents()));
                document.setUid(documentDTO.getStorageUID());
                document.setWartermark(documentDTO.getWatermark());

                for (DocumentMetadataDTO field : documentDTO.getMetadata())
                {
                    document.addMetadataAttribute(field.getKey(), field.getValue());
                }

                StorageResult result = storage.add(document);

                databaseDocumentDAO.updateStorageUID(documentDTO.getId(), document.getUid());
                databaseDocumentDAO.deleteFile(documentDTO.getId());
                databaseDocumentDAO.setDocumentAsUploaded(documentDTO.getId(), result.hash);

                //indexationClient.index(documentDTO.getStorageUID(), documentDTO);

                log.info("Document stored with UID " + document.getUid() + " for " + documentDTO.getFileName());
            }
            catch (Exception e)
            {
                log.error("Error adding document: ", e);
            }
        }
    }

}