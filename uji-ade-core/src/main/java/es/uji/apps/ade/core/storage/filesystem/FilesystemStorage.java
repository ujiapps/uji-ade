package es.uji.apps.ade.core.storage.filesystem;

import com.google.common.io.CountingInputStream;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.domains.StorageResult;
import es.uji.apps.ade.core.storage.Storage;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Component
public class FilesystemStorage implements Storage
{
    private String pathRoot;

    private static Logger log = LoggerFactory.getLogger(FilesystemStorage.class);

    @Autowired
    public FilesystemStorage(@Value("${uji.filesystem.path}") String pathRoot)
    {
        this.pathRoot = pathRoot;
    }

    private Path getDocumentPath(String uid)
    {
        log.info("getDocumentPath: " + uid);

        return Paths.get(pathRoot, uid.substring(0, 2), uid.substring(2, 4), uid);
    }

    @Override
    public StorageResult add(Document document) throws StorageIOException, StorageUnavailableException
    {
        Path path = getDocumentPath(document.getReference());

        if (document.getUid() != null && Files.exists(path))
        {
            overwriteMetadata(document);

            String hash = computeDigestFromPath(path);

            return new StorageResult(document.getReference(), hash);
        }

        createDirectories(document.getReference());
        String hash = writeContentToFile(path, document.getContents());
        overwriteMetadata(document);

        return new StorageResult(document.getReference(), hash);
    }

    private String computeDigestFromPath(Path path) {
        String hash = null;

        try {
            DigestInputStream digestInputStream = createDigestInputStream(Files.newInputStream(path));
            MessageDigest messageDigest = digestInputStream.getMessageDigest();
            hash = new String(Hex.encodeHex(messageDigest.digest()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return hash;
    }

    private DigestInputStream createDigestInputStream(InputStream data) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        return new DigestInputStream(data, messageDigest);
    }

    private String writeContentToFile(Path path, InputStream contents) throws StorageUnavailableException, StorageIOException
    {
        FileOutputStream fos;

        try
        {
            fos = new FileOutputStream(path.toFile());
        }
        catch (FileNotFoundException e)
        {
            throw new StorageUnavailableException();
        }

        try
        {
            DigestInputStream digestInputStream = createDigestInputStream(contents);

            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = digestInputStream.read(bytes)) != -1)
            {
                fos.write(bytes, 0, read);
            }

            fos.flush();
            fos.close();

            MessageDigest messageDigest = digestInputStream.getMessageDigest();
            return new String(Hex.encodeHex(messageDigest.digest()));
        }
        catch (Exception e)
        {
            throw new StorageIOException();
        }
    }

    private void createDirectories(String uid)
    {
        Path folder = Paths.get(pathRoot, uid.substring(0, 2), uid.substring(2, 4));

        if (Files.notExists(folder))
        {
            folder.toFile().mkdirs();
        }
    }

    @Override
    public void remove(String uid) throws StorageIOException
    {
        Path path = getDocumentPath(uid);
        try
        {
            Files.deleteIfExists(path);
            removeDirectoryIfEmtpy(path.getParent());
            removeDirectoryIfEmtpy(path.getParent().getParent());
        }
        catch (IOException e)
        {
            throw new StorageIOException();
        }
    }

    @Override
    public Boolean exists(String ref)
    {
        log.info("exists: " + ref);

        Path path = getDocumentPath(ref);

        log.info(path.toAbsolutePath().toString());

        return Files.exists(path);
    }

    private void removeDirectoryIfEmtpy(Path dir) throws StorageIOException
    {
        if (Files.isDirectory(dir) && dir.toFile().list().length == 0)
        {
            try
            {
                Files.delete(dir);
            }
            catch (IOException e)
            {
                throw new StorageIOException();
            }
        }
    }

    @Override
    public InputStream getByReference(String uid) throws StorageIOException
    {
        try
        {
            Path documentPath = getDocumentPath(uid);
            return new FileInputStream(documentPath.toFile());
        }
        catch (IOException e)
        {
            throw new StorageIOException();
        }
    }

    private void overwriteMetadata(Document document) throws StorageIOException
    {
        if (document.getRawMetadata() == null || document.getRawMetadata().size() == 0) return;

        Path path = getDocumentPath(document.getReference() + ".meta");

        try
        {
            Properties properties = new Properties();

            for (Map.Entry<String, List<String>> field : document.getRawMetadata().entrySet())
            {
                for (String value : field.getValue())
                {
                    properties.put(field.getKey(), value);
                }
            }

            properties.store(new FileOutputStream(path.toFile()), null);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new StorageIOException();
        }
    }
}