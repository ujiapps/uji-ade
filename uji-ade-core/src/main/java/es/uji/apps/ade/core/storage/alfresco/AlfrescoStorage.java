package es.uji.apps.ade.core.storage.alfresco;

import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.domains.StorageResult;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.commons.rest.StreamUtils;
import org.alfresco.cmis.client.AlfrescoDocument;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisInvalidArgumentException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisRuntimeException;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

public class AlfrescoStorage implements Storage
{
    private Logger log = LoggerFactory.getLogger(AlfrescoStorage.class);

    public static final String CMIS_DOCUMENT = "cmis:document";

    private Session session;
    private String workingPath;

    @Autowired
    public AlfrescoStorage(AlfrescoCredentials credentials,
                           @Value("${uji.alfresco.workingPath}") String workingPath)
    {
        Map<String, String> parameter = new HashMap<String, String>();
        parameter.put(SessionParameter.USER, credentials.getUserName());
        parameter.put(SessionParameter.PASSWORD, credentials.getPassword());
        parameter.put(SessionParameter.ATOMPUB_URL, credentials.getUrl());
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS,
                "org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl");

        SessionFactory factory = SessionFactoryImpl.newInstance();

        try
        {
            session = factory.getRepositories(parameter).get(0).createSession();
        }
        catch (CmisRuntimeException e)
        {
            log.error("Can't stablish connection with Alfresco Server");
        }

        this.workingPath = workingPath;
    }

    @Override
    public StorageResult add(Document document) throws StorageUnavailableException
    {
        if (session == null)
        {
            throw new StorageUnavailableException();
        }

        Folder workingFolder = (Folder) session.getObjectByPath(workingPath);
        Map<String, Object> properties = getAlfrescoPropertiesMap(document.getName(),
                document.getReference());

        InputStream dataStream = document.getContents();

        ContentStream contentStream = new ContentStreamImpl(document.getName(),
                new BigInteger("-1"), document.getMimeType(), dataStream);

        AlfrescoDocument alfrescoDocument = (AlfrescoDocument) workingFolder.createDocument(
                properties, contentStream, VersioningState.MAJOR);

        return new StorageResult(alfrescoDocument.getId(), null);
    }

    private Map<String, Object> getAlfrescoPropertiesMap(String name, String ref)
    {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put(PropertyIds.NAME, ref.substring(0, 5) + "_" + name);
        properties.put(PropertyIds.OBJECT_TYPE_ID, CMIS_DOCUMENT);

        return properties;
    }

    @Override
    public void remove(String ref)
    {
        CmisObject document = session.getObject(ref);
        document.delete(true);
    }

    @Override
    public Boolean exists(String ref)
    {
        try
        {
            session.getObject(ref);
            return true;
        }
        catch (CmisInvalidArgumentException e)
        {
            return false;
        }
    }

    @Override
    public InputStream getByReference(String uid) throws StorageIOException
    {
        org.apache.chemistry.opencmis.client.api.Document alfrescoDocument;
        alfrescoDocument = (org.apache.chemistry.opencmis.client.api.Document) session.getObject(uid);

        return alfrescoDocument.getContentStream().getStream();
    }
}
