package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class StorageUnavailableException extends Exception
{
    public StorageUnavailableException()
    {
        super("The storage backend is currently unavailable.");
    }
}
