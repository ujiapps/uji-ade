package es.uji.apps.ade.core.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.ade.core.model.db.*;
import es.uji.apps.ade.core.model.domains.DocumentStatus;
import es.uji.apps.ade.core.model.domains.StorageType;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DatabaseDocumentDAO extends BaseDAODatabaseImpl
{
    private QDocumentDTO qDocumentDTO = QDocumentDTO.documentDTO;
    private QDocumentMetadataDTO qDocumentMetadataDTO = QDocumentMetadataDTO.documentMetadataDTO;
    private QDocumentReferenceDTO qDocumentReferenceDTO = QDocumentReferenceDTO.documentReferenceDTO;

    public List<Long> getPendingIdsToMigrate()
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .where(qDocumentDTO.statusId.eq(DocumentStatus.PENDIENTE_SUBIR.getId()))
                .list(qDocumentDTO.id);
    }

    public DocumentDTO getPendingFileToMigrateById(Long documentId)
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .leftJoin(qDocumentDTO.metadata, qDocumentMetadataDTO)
                .fetch()
                .where(qDocumentDTO.id.eq(documentId)
                        .and(qDocumentDTO.statusId.eq(DocumentStatus.PENDIENTE_SUBIR.getId())))
                .uniqueResult(qDocumentDTO);
    }

    public List<DocumentDTO> getPendingFilesToRemove()
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .where(qDocumentDTO.storageUID.isNotEmpty()
                        .and(qDocumentDTO.statusId.eq(DocumentStatus.PENDIENTE_BORRAR.getId())))
                .list(qDocumentDTO);
    }

    @Transactional
    public void updateStorageUID(Long id, String storageUID)
    {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.id.eq(id))
                .set(qDocumentDTO.storageUID, storageUID)
                .execute();
    }

    @Transactional
    public void updateStorageUID(String reference, String storageUID)
    {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.reference.eq(reference))
                .set(qDocumentDTO.storageUID, storageUID)
                .execute();
    }

    @Transactional
    public void deleteFile(Long id)
    {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.id.eq(id))
                .set(qDocumentDTO.fileContents, new byte[0])
                .execute();
    }

    @Transactional
    public void setDocumentAsUploaded(Long id, String hash)
    {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.id.eq(id))
                .set(qDocumentDTO.statusId, DocumentStatus.SUBIDO.getId())
                .set(qDocumentDTO.hash, hash)
                .execute();
    }

    @Transactional
    public void setDocumentAsDeleted(Long id)
    {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.id.eq(id))
                .set(qDocumentDTO.statusId, DocumentStatus.BORRADO.getId())
                .set(qDocumentDTO.storageUID, "")
                .execute();
    }

    public DocumentDTO getPublishedByReference(String reference)
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .leftJoin(qDocumentDTO.metadata)
                .fetch()
                .where(qDocumentDTO.reference.eq(reference))
                .uniqueResult(qDocumentDTO);
    }

    public DocumentDTO getByReference(String reference)
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .where(qDocumentDTO.reference.eq(reference))
                .uniqueResult(qDocumentDTO);
    }

    public List<DocumentDTO> getMigratedAlfrescoDocuments()
    {
        return new JPAQuery(entityManager).from(qDocumentDTO)
                .where(qDocumentDTO.storageUID.isNotEmpty()
                        .and(qDocumentDTO.statusId.eq(DocumentStatus.SUBIDO.getId()))
                        .and(qDocumentDTO.storageTypeId.eq(StorageType.ALFRESCO.getId())))
                .list(qDocumentDTO);
    }

    public List<DocumentReferenceDTO> getReferences(String scope, String hash)
    {
        return new JPAQuery(entityManager).from(qDocumentReferenceDTO)
                .where(qDocumentReferenceDTO.scope.equalsIgnoreCase(scope).and(qDocumentReferenceDTO.hash.eq(hash)))
                .list(qDocumentReferenceDTO);
    }

    @Transactional
    public void updateWatermark(String reference, String waterMark) {
        new JPAUpdateClause(entityManager, qDocumentDTO).where(qDocumentDTO.reference.eq(reference))
                .set(qDocumentDTO.watermark, waterMark)
                .execute();
    }
}
