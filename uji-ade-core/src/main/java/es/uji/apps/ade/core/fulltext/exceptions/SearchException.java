package es.uji.apps.ade.core.fulltext.exceptions;

import java.rmi.server.ExportException;

public class SearchException extends ExportException
{
    public SearchException(String message)
    {
        super(message);
    }
}
