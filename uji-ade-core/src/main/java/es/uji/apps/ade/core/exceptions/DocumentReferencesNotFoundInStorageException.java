package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class DocumentReferencesNotFoundInStorageException extends Exception
{
    public DocumentReferencesNotFoundInStorageException()
    {
        super("The requested document references couldn't be found in the storage system");
    }
}
