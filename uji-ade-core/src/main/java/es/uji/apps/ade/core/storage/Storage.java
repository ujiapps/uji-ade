package es.uji.apps.ade.core.storage;

import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.domains.StorageResult;

import java.io.InputStream;
import java.util.Map;

public interface Storage
{
    StorageResult add(Document document) throws StorageIOException, StorageUnavailableException;

    void remove(String reference) throws StorageIOException;

    Boolean exists(String reference);

    InputStream getByReference(String reference) throws StorageIOException;
}
