package es.uji.apps.ade.core.services;

import es.uji.apps.ade.core.dao.DatabaseDocumentDAO;
import es.uji.apps.ade.core.exceptions.*;
import es.uji.apps.ade.core.model.Metadata;
import es.uji.apps.ade.core.model.db.DocumentDTO;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.DocumentFactory;
import es.uji.apps.ade.core.model.db.DocumentReferenceDTO;
import es.uji.apps.ade.core.model.domains.DocumentStatus;
import es.uji.apps.ade.core.model.domains.TransformType;
import es.uji.apps.ade.core.model.transforms.TransformList;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.commons.rest.UIEntity;
import org.jsoup.select.Collector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Service
public class StorageService
{
    private Storage storage;
    private DatabaseDocumentDAO databaseDocumentDAO;
    private MetadataService metadataService;

    @Autowired
    public StorageService(Storage storage, DatabaseDocumentDAO databaseDocumentDAO, MetadataService metadataService)
    {
        this.storage = storage;
        this.databaseDocumentDAO = databaseDocumentDAO;
        this.metadataService = metadataService;
    }

    public List<Document> retriveDocumentsToExport(String scope, String hash)
            throws DocumentReferencesNotFoundInStorageException, StorageIOException, DocumentNotFoundException,
            InvalidTransformParameterException
    {
        List<DocumentReferenceDTO> documentReferences = databaseDocumentDAO.getReferences(scope, hash);
        List<Document> documents = new ArrayList<>();

        if (documentReferences == null || documentReferences.isEmpty())
        {
            throw new DocumentReferencesNotFoundInStorageException();
        }

        for (DocumentReferenceDTO documentReference : documentReferences)
        {
            documents.add(retrieveDocument(documentReference.getReference()));
        }

        return documents;
    }

    public Document retrieveDocument(String reference)
            throws InvalidTransformParameterException, DocumentNotFoundException, StorageIOException
    {
        return retrieveDocument(reference, new TransformList());
    }

    public Document retrieveDocument(String reference, TransformList transforms)
            throws StorageIOException, InvalidTransformParameterException, DocumentNotFoundException
    {
        Document document = retrieveDocumentInfo(reference);
        retrieveDocumentStream(reference, document);
        waterMarkDocument(transforms, document);

        return transforms.apply(document);
    }

    private void waterMarkDocument(TransformList transforms, Document document)
    {
        if (!document.hasWaterMark()) return;

        transforms.add(TransformType.WATERMARK, Collections.emptyList());
    }

    private void retrieveDocumentStream(String reference, Document document)
            throws StorageIOException
    {
        if (!storage.exists(reference)) return;

        InputStream documentStream = storage.getByReference(reference);
        document.setContents(documentStream);
    }

    public DocumentDTO storeDocument(String nombre, String reference, String mimeType, byte[] contents)
    {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setFileContents(contents);
        documentDTO.setFileName(nombre);
        documentDTO.setMimeType(mimeType);

        if (reference != null)
        {
            documentDTO.setReference(reference);
        }

        return databaseDocumentDAO.insert(documentDTO);
    }

    public void markDocumentAsPendingToDelete(String ref)
    {
        DocumentDTO documentDTO = databaseDocumentDAO.getByReference(ref);
        documentDTO.setStatusId(DocumentStatus.PENDIENTE_BORRAR.getId());

        databaseDocumentDAO.update(documentDTO);
    }

    private Document retrieveDocumentInfo(String reference)
            throws DocumentNotFoundException
    {
        DocumentDTO documentDTO = databaseDocumentDAO.getPublishedByReference(reference);

        failIfDocumentDoesntExist(documentDTO);

        Document document = DocumentFactory.build(reference, documentDTO.getMimeType());
        document.setUid(documentDTO.getStorageUID());
        document.setName(documentDTO.getFileName());
        document.setMimeType(documentDTO.getMimeType());

        if (documentDTO.getFileContents() != null)
        {
            document.setContents(new ByteArrayInputStream(documentDTO.getFileContents()));
        }

        document.setWartermark(documentDTO.getWatermark());

        UIEntity entity = metadataService.retrieveMetadata(reference);

        for (Entry<String, List<String>> entry : entity.entrySet())
        {
            for (String value : entry.getValue())
            {
                document.addMetadataAttribute(entry.getKey(), value);
            }
        }

        return document;
    }

    private void failIfDocumentDoesntExist(DocumentDTO documentDTO)
            throws DocumentNotFoundException
    {
        if (documentDTO == null)
        {
            throw new DocumentNotFoundException();
        }
    }

    public void updateWatermark(String reference, String waterMark) {
        databaseDocumentDAO.updateWatermark(reference, waterMark);
    }
}
