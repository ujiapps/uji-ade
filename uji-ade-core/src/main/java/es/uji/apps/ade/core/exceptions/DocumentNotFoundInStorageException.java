package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class DocumentNotFoundInStorageException extends Exception
{
    public DocumentNotFoundInStorageException()
    {
        super("The requested document couldn't be found in the storage system");
    }
}
