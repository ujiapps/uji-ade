package es.uji.apps.ade.core.model;

import java.util.*;

public class Metadata
{
    private Map<String, List<String>> values;

    public Metadata()
    {
        this.values = new HashMap<>();
    }

    public void addField(String key, String value)
    {
        if (!values.containsKey(key))
        {
            values.put(key, new ArrayList<>());
        }

        List<String> metaValues = values.get(key);
        metaValues.add(value);

        values.put(key, metaValues);
    }

    public Map<String, List<String>> getRawValues()
    {
        return Collections.unmodifiableMap(values);
    }
}
