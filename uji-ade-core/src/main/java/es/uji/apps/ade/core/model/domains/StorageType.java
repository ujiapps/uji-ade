package es.uji.apps.ade.core.model.domains;

public enum StorageType
{
    ALFRESCO("AL"), FILESYSTEM("FS");

    private String id;

    StorageType(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
}
