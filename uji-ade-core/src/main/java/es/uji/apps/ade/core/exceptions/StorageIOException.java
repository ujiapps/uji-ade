package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class StorageIOException extends Exception
{
    public StorageIOException()
    {
        super("The document can't be retrieved from the storage.");
    }
}
