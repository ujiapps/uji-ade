package es.uji.apps.ade.core.media.pdf;

@SuppressWarnings("serial")
public class FingerPrintAddException extends Exception
{
    public FingerPrintAddException(Exception e)
    {
        super("Can not add the fingerprint message to the PDF document", e);
    }
}
