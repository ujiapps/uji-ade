package es.uji.apps.ade.core.model.domains;

public class StorageResult
{
    public String uuid;
    public String hash;

    public StorageResult(String uuid, String hash)
    {
        this.uuid = uuid;
        this.hash = hash;
    }
}
