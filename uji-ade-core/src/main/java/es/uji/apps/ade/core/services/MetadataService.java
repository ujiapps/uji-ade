package es.uji.apps.ade.core.services;

import es.uji.apps.ade.core.dao.DatabaseDocumentDAO;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.db.DocumentDTO;
import es.uji.apps.ade.core.model.db.DocumentMetadataDTO;
import es.uji.apps.ade.core.model.domains.DocumentStatus;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.PathParam;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Service
public class MetadataService
{
    private DatabaseDocumentDAO databaseDocumentDAO;

    @Autowired
    public MetadataService(DatabaseDocumentDAO databaseDocumentDAO)
    {
        this.databaseDocumentDAO = databaseDocumentDAO;
    }

    @Transactional
    public void overwriteMetadata(String reference, Map<String, List<String>> metadata)
    {
        DocumentDTO documentDTO = databaseDocumentDAO.getPublishedByReference(reference);
        documentDTO.setStatusId(DocumentStatus.PENDIENTE_SUBIR.getId());

        persistMetadataField(documentDTO, metadata);
    }

    @Transactional
    protected void persistMetadataField(DocumentDTO documentDTO, Map<String, List<String>> metadata)
    {
        Set<DocumentMetadataDTO> metadataFields = new HashSet<>();

        for (Map.Entry<String, List<String>> entry : metadata.entrySet())
        {
            for (String value : entry.getValue())
            {
                DocumentMetadataDTO metadataField = new DocumentMetadataDTO();
                metadataField.setKey(entry.getKey());
                metadataField.setValue(value);
                metadataField.setDocument(documentDTO);

                metadataFields.add(metadataField);
            }
        }

        documentDTO.setMetadata(metadataFields);

        databaseDocumentDAO.update(documentDTO);
    }

    public UIEntity retrieveMetadata(String reference)
    {
        DocumentDTO documentDTO = databaseDocumentDAO.getPublishedByReference(reference);

        if (documentDTO == null)
            return new UIEntity();

        Set<DocumentMetadataDTO> metadataFrom = documentDTO.getMetadata();

        UIEntity result = new UIEntity();

        for (DocumentMetadataDTO field : metadataFrom)
        {
            if (result.get(field.getKey()) == null)
            {
                result.put(field.getKey(), new ArrayList());
            }

            List<String> updated = result.getArray(field.getKey());
            updated.add(field.getValue());

            result.put(field.getKey(), updated);
        }

        return result;
    }
}