package es.uji.apps.ade.core.storage;

import es.uji.apps.ade.core.fulltext.ElasticSearchClient;
import es.uji.apps.ade.core.fulltext.exceptions.IndexationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndexationClient
{
    private ElasticSearchClient searchClient;

    @Autowired
    public IndexationClient(ElasticSearchClient searchClient)
    {
        this.searchClient = searchClient;
    }

    public void delete(String uuid)
    {
        searchClient.remove(uuid);
    }

    public String index(String uuid, Object document) throws IndexationException
    {
        return searchClient.add(uuid, document);
    }
}
