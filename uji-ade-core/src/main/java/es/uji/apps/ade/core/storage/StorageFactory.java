package es.uji.apps.ade.core.storage;

import es.uji.apps.ade.core.model.domains.StorageType;
import es.uji.apps.ade.core.storage.alfresco.AlfrescoStorage;
import es.uji.apps.ade.core.storage.filesystem.FilesystemStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class StorageFactory
{
    private static ApplicationContext applicationContext;

    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext)
    {
        StorageFactory.applicationContext = applicationContext;
    }

    public static Storage build(String storageTypeId)
    {
        if (storageTypeId.equals(StorageType.ALFRESCO.getId())) {
            return applicationContext.getBean(AlfrescoStorage.class);
        }

        return applicationContext.getBean(FilesystemStorage.class);
    }
}
