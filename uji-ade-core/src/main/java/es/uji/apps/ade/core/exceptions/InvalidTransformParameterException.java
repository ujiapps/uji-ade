package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class InvalidTransformParameterException extends Exception
{
    public InvalidTransformParameterException()
    {
        super("The transform parameter provided is invalid");
    }
}
