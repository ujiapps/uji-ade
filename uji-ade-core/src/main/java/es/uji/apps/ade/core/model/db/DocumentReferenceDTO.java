package es.uji.apps.ade.core.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "ADE_EXT_DESCARGAS_ZIP")
@Entity
public class DocumentReferenceDTO
{
    @Column(name = "CLAVE")
    private String key;

    @Column(name = "AMBITO")
    private String scope;

    private String hash;

    @Id
    @Column(name = "REFERENCIA")
    private String reference;

    public String getScope()
    {
        return scope;
    }

    public void setScope(String scope)
    {
        this.scope = scope;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getHash()
    {
        return hash;
    }

    public void setHash(String hash)
    {
        this.hash = hash;
    }
}