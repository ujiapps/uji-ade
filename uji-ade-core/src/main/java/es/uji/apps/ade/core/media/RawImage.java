package es.uji.apps.ade.core.media;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class RawImage
{
    private BufferedImage sourceImage;

    public RawImage(InputStream stream) throws IOException
    {
        sourceImage = ImageIO.read(stream);
    }

    public int getWidth()
    {
        return sourceImage.getWidth();
    }

    public int getHeight()
    {
        return sourceImage.getHeight();
    }
}
