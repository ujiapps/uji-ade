package es.uji.apps.ade.core.exceptions;

@SuppressWarnings("serial")
public class DatabaseDocumentCreationException extends Exception
{
    public DatabaseDocumentCreationException()
    {
        super("The requested document can't be created");
    }
}
