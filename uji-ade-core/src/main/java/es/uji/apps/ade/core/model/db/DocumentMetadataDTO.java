package es.uji.apps.ade.core.model.db;

import javax.persistence.*;

@Table(name = "ADE_DOCUMENTOS_METADATOS")
@Entity
public class DocumentMetadataDTO
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "DOCUMENT_ID")
    private DocumentDTO document;

    private String key;
    private String value;

    public DocumentMetadataDTO()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public DocumentDTO getDocument()
    {
        return document;
    }

    public void setDocument(DocumentDTO document)
    {
        this.document = document;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
}