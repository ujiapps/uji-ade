package es.uji.apps.ade.core.model;

public class DocumentFactory
{
    public static Document build(String reference, String mimetype)
    {
        if (mimetype.startsWith("image/"))
        {
            return new ImageDocument(reference);
        }

        return new Document(reference);
    }
}
