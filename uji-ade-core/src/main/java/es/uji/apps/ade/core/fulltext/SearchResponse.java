package es.uji.apps.ade.core.fulltext;

import java.util.ArrayList;
import java.util.List;

public class SearchResponse<T>
{
    private Long total;
    private List<T> records;

    public SearchResponse()
    {
        this.records = new ArrayList<>();
    }

    public Long getTotal()
    {
        return total;
    }

    public void setTotal(Long total)
    {
        this.total = total;
    }

    public List<T> getRecords()
    {
        return records;
    }

    public void setRecords(List<T> records)
    {
        this.records = records;
    }

    public void addRecord(T record)
    {
        records.add(record);
    }
}
