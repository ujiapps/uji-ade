package es.uji.apps.ade.core.storage.notstored;

import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.domains.StorageResult;
import es.uji.apps.ade.core.storage.Storage;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class NotStoredStorage implements Storage
{
    @Override
    public StorageResult add(Document document) throws StorageIOException, StorageUnavailableException
    {
        return new StorageResult(
            document.getReference(),
            "693ddcb4a444b80717c649e458f8c48cd2395a4d561c27943aed8e0fa866fde8"
        );
    }

    @Override
    public void remove(String ref) throws StorageIOException
    {
    }

    @Override
    public Boolean exists(String ref)
    {
        return false;
    }

    @Override
    public InputStream getByReference(String reference) throws StorageIOException
    {
        return new ByteArrayInputStream(new byte[0]);
    }
}
