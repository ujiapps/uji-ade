package es.uji.apps.ade.core.model;

import java.util.Random;

public class ReferenceGenerator
{
    private static final Integer REFERENCE_LENGTH = 32;

    public String generate()
    {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

        StringBuilder sb = new StringBuilder();
        Random random = new Random();

        for (int i = 0; i < REFERENCE_LENGTH; i++)
        {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }

        return sb.toString();
    }
}
