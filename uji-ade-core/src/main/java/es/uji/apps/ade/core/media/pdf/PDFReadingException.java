package es.uji.apps.ade.core.media.pdf;

import java.io.IOException;

@SuppressWarnings("serial")
public class PDFReadingException extends Exception
{
    public PDFReadingException(IOException e)
    {
        super("Can not open and read the selected PDF", e);
    }
}
