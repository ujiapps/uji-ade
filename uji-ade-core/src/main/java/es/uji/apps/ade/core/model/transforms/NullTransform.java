package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;

import java.util.List;

public class NullTransform extends Transform
{
    public NullTransform(List<TransformParam> params)
    {
        super(params);
    }

    @Override
    public Document apply(Document document) throws StorageIOException
    {
        return document;
    }
}
