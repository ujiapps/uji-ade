package es.uji.apps.ade.core.fulltext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.LoggingFilter;
import es.uji.apps.ade.core.fulltext.exceptions.IndexationException;
import es.uji.apps.ade.core.fulltext.exceptions.SearchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;

@Component
public class ElasticSearchClient
{
    private final WebResource resource;
    private final ObjectMapper mapper;

    private String index;
    private String type;

    @Autowired
    public ElasticSearchClient(
            @Value("${uji.fulltext.host}") String host,
            @Value("${uji.fulltext.port}") String port,
            @Value("${uji.fulltext.index}") String index,
            @Value("${uji.fulltext.type}") String type)
    {
        this.index = index;
        this.type = type;

        Client client = Client.create();
        client.addFilter(new LoggingFilter());
        resource = client.resource("http://" + host + ":" + port);

        this.mapper = new ObjectMapper();
    }

    public void createIndex()
    {
        ObjectNode analysis = node();
        analysis.set("analyzer",
                node().set("default",
                        node()
                                .put("tokenizer", "standard")
                                .set("filter", array()
                                        .add("standard")
                                        .add("asciifoldingpreserved")
                                        .add("lowercase"))));
        analysis.set("filter",
                node().set("asciifoldingpreserved",
                        node()
                                .put("type", "asciifolding")
                                .put("preserve_original", true)));

        ObjectNode configuration = node();
        configuration.set("settings",
                node().set("analysis", analysis));

        resource.path("/" + index)
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, configuration);
    }

    private ArrayNode array()
    {
        return mapper.createArrayNode();
    }

    private ObjectNode node()
    {
        return mapper.createObjectNode();
    }

    private String indexDocument(String uuid, Object bean, boolean refresh) throws IndexationException
    {
        String uuidFilter = (uuid != null && !uuid.isEmpty()) ? "/" + uuid : "";

        WebResource path = resource.path("/" + index + "/" + type + uuidFilter);

        if (refresh)
        {
            path = path.queryParam("refresh", "true");
        }

        WebResource.Builder builder = path.type(MediaType.APPLICATION_JSON);

        ClientResponse response;
        byte[] content = generateContent(bean);

        if (uuidFilter.isEmpty())
        {
            response = builder.post(ClientResponse.class, content);
        }
        else
        {
            response = builder.put(ClientResponse.class, content);
        }

        checkIndexationResponse(response);

        JsonNode jsonResponse = response.getEntity(JsonNode.class);

        return jsonResponse.get("_id").asText();
    }

    public String add(Object bean) throws IndexationException
    {
        return indexDocument(null, bean, false);
    }

    public String addSync(Object bean) throws IndexationException
    {
        return indexDocument(null, bean, true);
    }

    public String add(String uuid, Object bean) throws IndexationException
    {
        return indexDocument(uuid, bean, false);
    }

    public String addSync(String uuid, Object bean) throws IndexationException
    {
        return indexDocument(uuid, bean, true);
    }

    public void enableRichContentFor(String field)
    {
        ObjectNode fields = node();
        fields.set("name", createFlag("store", "yes"));
        fields.set("content", createFlag("store", "yes"));
        fields.set("content_type", createFlag("store", "yes"));

        JsonNode configuration = node()
                .set(type, node()
                        .set("properties",
                                node().set(field,
                                        node()
                                                .put("type", "attachment")
                                                .set("fields", fields))));

        resource.path("/" + index + "/" + type + "/_mapping")
                .type(MediaType.APPLICATION_JSON)
                .put(ClientResponse.class, configuration);
    }

    private JsonNode createFlag(String field, String value)
    {
        ObjectNode flag = node();
        flag.put(field, value);

        return flag;
    }

    private void checkIndexationResponse(ClientResponse response) throws IndexationException
    {
        if (response.getStatus() != 200 && response.getStatus() != 201)
        {
            throw new IndexationException("Object has not been created");
        }
    }

    private byte[] generateContent(Object bean) throws IndexationException
    {
        try
        {
            return mapper.writeValueAsBytes(bean);
        }
        catch (JsonProcessingException e)
        {
            throw new IndexationException("Object to index is not a Java Bean");
        }
    }

    public <T> SearchResponse<T> search(Class<T> clazz, String field, String value) throws SearchException
    {
        return search(clazz, field, value, Collections.EMPTY_LIST);
    }

    public <T> SearchResponse<T> search(Class<T> clazz, String field, String value, List<String> fields) throws SearchException
    {
        ObjectNode matchExpression = node();
        matchExpression.put(field, value);

        ObjectNode queryDetail = node();
        queryDetail.put("match", matchExpression);

        ObjectNode query = node();

        if (fields != null && !fields.isEmpty())
        {
            ArrayNode fieldList = array();
            fields.stream().forEach(f -> fieldList.add(f));

            query.set("_source", fieldList);
        }

        query.set("query", queryDetail);

        ClientResponse response = resource.path("/" + index + "/" + type + "/_search")
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, query);

        JsonNode result = response.getEntity(JsonNode.class);

        SearchResponse<T> searchResponse = new SearchResponse<>();
        searchResponse.setTotal(result.get("hits").get("total").asLong());

        for (JsonNode hits : result.path("hits").get("hits"))
        {
            JsonNode source = hits.get("_source");

            try
            {
                T bean = mapper.treeToValue(source, clazz);
                searchResponse.addRecord(bean);
            }
            catch (JsonProcessingException e)
            {
                throw new SearchException("Object to return from search doesn't match the defined Java Bean");
            }
        }

        return searchResponse;
    }

    public void dropIndex()
    {
        resource.path("/" + index).delete(ClientResponse.class);
    }

    private void refresh()
    {
        resource.path("/" + index + "/_refresh").post(ClientResponse.class);
    }

    public <T> T get(Class<T> clazz, String uuid) throws SearchException
    {
        ClientResponse response = resource.path("/" + index + "/" + type + "/" + uuid)
                .get(ClientResponse.class);

        JsonNode result = response.getEntity(JsonNode.class);

        try
        {
            return mapper.treeToValue(result.get("_source"), clazz);
        }
        catch (JsonProcessingException e)
        {
            throw new SearchException("Object to retrieved by uuid doesn't match the defined Java Bean");
        }
    }

    public void remove(String reference)
    {
    }
}