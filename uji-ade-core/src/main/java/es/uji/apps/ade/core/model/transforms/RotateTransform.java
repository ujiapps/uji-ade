package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.ImageDocument;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class RotateTransform extends Transform
{
    public static final Float ROTATE_DEGREES = 90.0F;
    public static final String DEGREES_PARAM = "d";

    public RotateTransform(List<TransformParam> params)
    {
        super(params);
    }

    @Override
    public Document apply(Document document) throws StorageIOException,
            InvalidTransformParameterException
    {
        if (notApplies(document))
        {
            return document;
        }

        Document rotatedDocument = document.clone();
        rotatedDocument.setContents(new ByteArrayInputStream(rotateContent(document.getContents())));

        return rotatedDocument;
    }

    private boolean notApplies(Document document)
    {
        return !(document instanceof ImageDocument || document.getMimeType().startsWith("image/"));
    }

    private byte[] rotateContent(InputStream contents) throws StorageIOException,
            InvalidTransformParameterException
    {
        Float rotation = getDesiredRotation();

        try
        {
            BufferedImage sourceImage = ImageIO.read(contents);
            Integer width = sourceImage.getWidth();
            Integer height = sourceImage.getHeight();
            BufferedImage newImage = new BufferedImage(height, width, sourceImage.getType());

            Graphics2D graphics = (Graphics2D) newImage.getGraphics();
            graphics.rotate(Math.toRadians(rotation), newImage.getWidth() / 2,
                    newImage.getHeight() / 2);
            graphics.translate((newImage.getWidth() - sourceImage.getWidth()) / 2,
                    (newImage.getHeight() - sourceImage.getHeight()) / 2);
            graphics.drawImage(sourceImage, 0, 0, sourceImage.getWidth(), sourceImage.getHeight(),
                    null);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(newImage, "jpg", baos);
            return baos.toByteArray();
        }
        catch (IOException e)
        {
            throw new StorageIOException();
        }
    }

    public Float getDesiredRotation() throws InvalidTransformParameterException
    {
        Float rotation = ROTATE_DEGREES;

        if (params.size() == 1)
        {
            TransformParam param = getParam(DEGREES_PARAM);
            rotation = param.getFloatValue();
        }

        return rotation;
    }
}