package es.uji.apps.ade.core.storage.alfresco;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AlfrescoCredentials
{
    private String server;
    private String url;
    private String userName;
    private String password;

    @Autowired
    public AlfrescoCredentials(@Value("${uji.alfresco.server}") String server,
                               @Value("${uji.alfresco.url}") String url,
                               @Value("${uji.alfresco.username}") String userName,
                               @Value("${uji.alfresco.password}") String password)
    {
        this.server = server;
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    public String getUrl()
    {
        return url;
    }

    public String getUserName()
    {
        return userName;
    }

    public String getPassword()
    {
        return password;
    }
}
