package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.media.pdf.PDFFingerPrintStamper;
import es.uji.apps.ade.core.model.Document;
import es.uji.commons.rest.StreamUtils;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

public class WatermarkTransform extends Transform
{
    public WatermarkTransform(List<TransformParam> params)
    {
        super(params);
    }

    @Override
    public Document apply(Document document)
    {
        if (notApplies(document))
        {
            return document;
        }

        try
        {
            byte[] pdf = StreamUtils.inputStreamToByteArray(document.getContents());
            PDFFingerPrintStamper pdfFingerPrintStamper = new PDFFingerPrintStamper(pdf);

            byte[] waterMarkedPdf = pdfFingerPrintStamper.writeFooterMessage(document.getWartermark());

            document.setContents(new ByteArrayInputStream(waterMarkedPdf));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return document;
    }

    private boolean notApplies(Document document)
    {
        return !applies(document);
    }

    private boolean applies(Document document)
    {
        return document.getMimeType().equalsIgnoreCase("application/pdf") && document.getWartermark() != null;
    }

    public static void main(String[] args) throws Exception
    {
        //byte[] pdf = StreamUtils.inputStreamToByteArray(new FileInputStream("/mnt/dbobjects/alma/DI/BW/DIBWOJK6SI1JC5TXEQPAA9PBW8GZ5VF5"));
        //byte[] pdf = StreamUtils.inputStreamToByteArray(new FileInputStream("/mnt/dbobjects/alma/GG/7E/GG7EB2Z4MMKG3RT9MBWFMUO1SVLTZNFL"));
        byte[] pdf = StreamUtils.inputStreamToByteArray(new FileInputStream("/mnt/dbobjects/alma/C7/UV/C7UVUIBURLAPVKVYWY6CN4IQKCEZISRX"));


        Document document = new Document("DIBWOJK6SI1JC5TXEQPAA9PBW8GZ5VF5");
        document.setContents(new ByteArrayInputStream(pdf));
        document.setMimeType("application/pdf");
        document.setWartermark(
            "Copia auténtica del documento firmado electrónicamente por Cristina Pauner Chulvi como secretario/a y sellado electrónicamente por la Universidad el 09/05/2019 a las 05.32 h con el beneplácito de\n" +
            "la presidencia. Se puede comprobar su autenticidad accediendo a la dirección http://www.uji.es/documents e introduciendo el código seguro de verificación 5F0DCD69C4C77C3B672D."
        );

        WatermarkTransform transform = new WatermarkTransform(Collections.EMPTY_LIST);
        Document waterMarkedDocument = transform.apply(document);

        FileOutputStream fos = new FileOutputStream("/tmp/watermark.pdf");
        fos.write(StreamUtils.inputStreamToByteArray(waterMarkedDocument.getContents()));
        fos.flush();
        fos.close();
    }
}
