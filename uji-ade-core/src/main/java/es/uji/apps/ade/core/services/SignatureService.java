package es.uji.apps.ade.core.services;

import es.gob.afirma.core.signers.AOSignConstants;
import es.gob.afirma.core.signers.AOSigner;
import es.gob.afirma.signers.pades.AOPDFSigner;
import es.gob.afirma.signers.tsp.pkcs7.TsaParams;
import es.uji.commons.rest.StreamUtils;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyStore;
import java.util.Properties;

@Service
public class SignatureService
{
    private static final String CERT_PASS = "axJSyXkY";
    private static final String CERT_ALIAS = "1";

    public byte[] signPdf(byte[] data) throws Exception
    {
        final KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream("/etc/uji/eujier.p12"), CERT_PASS.toCharArray());

        KeyStore.PasswordProtection passwordProtection = new KeyStore.PasswordProtection(CERT_PASS.toCharArray());
        KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry(CERT_ALIAS, passwordProtection);
        final AOSigner signer = new AOPDFSigner();

        final Properties extraParams = new Properties();
        extraParams.put("tsaURL", "http://psis.catcert.net/psis/catcert/tsp");
        extraParams.put("tsaPolicy", "0.4.0.2023.1.1");
        extraParams.put("tsaRequireCert", true);
        extraParams.put("tsaHashAlgorithm", "SHA-512");
        extraParams.put("tsType", TsaParams.TS_SIGN_DOC);
        extraParams.put("certificationLevel", "1");  
        extraParams.put("policyIdentifier", "urn:oid:2.16.724.1.3.1.1.2.1.9");  
        extraParams.put("policyQualifier", "https://sede.060.gob.es/politica_de_firma_anexo_1.pdf");  
        extraParams.put("policyIdentifierHashAlgorithm", "http://www.w3.org/2000/09/xmldsig#sha1");  
        extraParams.put("policyIdentifierHash", "G7roucf600+f03r/o0bAOQ6WAs0=");
        
        return signer.sign(
                data,
                AOSignConstants.SIGN_ALGORITHM_SHA512WITHRSA,
                privateKeyEntry.getPrivateKey(),
                privateKeyEntry.getCertificateChain(),
                extraParams
        );
    }

    public static void main(String[] args) throws Exception
    {
        byte[] data = StreamUtils.inputStreamToByteArray(new FileInputStream("uji-ade-core/src/main/resources/in.pdf"));
        SignatureService signatureService = new SignatureService();
        byte[] signedData = signatureService.signPdf(data);

        FileOutputStream fos = new FileOutputStream("uji-ade-core/src/main/resources/out.pdf");
        fos.write(signedData);
        fos.flush();
        fos.close();
    }
}
