package es.uji.apps.ade.core.model;

public class ImageDocument extends Document
{
    public ImageDocument(String reference)
    {
        super(reference);
    }

    @Override
    public Document clone()
    {
        ImageDocument cloned = new ImageDocument(reference);
        initValues(cloned);

        return cloned;
    }
}
