package es.uji.apps.ade.core.media.pdf;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class PDFFingerPrintStamper
{
    private PdfReader reader;
    private final float pageHeight;

    private static String LOGO_UJI_IMG =
            "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsK" +
            "CwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQU" +
            "FBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAA8ADwDASIA" +
            "AhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAACAkGBwABBQMC/8QAOBAAAQMDAwIEBAQCCwAAAAAA" +
            "AQIDBAUGEQAHEgghEyIxQQkUUWEVIzJCUrEncYGRkpOho8HR8f/EABYBAQEBAAAAAAAAAAAAAAAA" +
            "AAEAAv/EABcRAQEBAQAAAAAAAAAAAAAAAAABESH/2gAMAwEAAhEDEQA/AGohWfbW9A7ZO9/VxuRI" +
            "lSqNtvbtPohdWYsuuodieI1k8CEqc5nI9+ONQLezrV6l9gJcVi8bEtqnNygfAmstOPxnceoDiXcZ" +
            "+xwftrOox8kAd+2sBB9O+lDPfFc3jcz4dOtdr7CC6cf3u651a+JV1APNhQfp1ISpAWlTNHGCk+iv" +
            "zOXbVpw4rP21Wm7fyUOfQ6lOuOTbbbIkMofp6GnJbq1cDwbbWw6V9mz2QkK9O+CRpWdr/Ei35j1W" +
            "My9XqfU0yHkt+FMpbIHmIAA4BJHr9dNWvWbVHH6DEjUaLXkSmHXH4Ul5DLalJDeCFKQvuApeAMHH" +
            "fPbWg6e2sp+ZbDLz0isS/FWtbT9cbablLbJylRQ2lKQCMEeUH6gal2qluFF+0zboLtgUW2qtHkqc" +
            "eFxy3ZkZmMlJJw4MH+H1wAO2obQpXUPcFHiVOn1/beowZbYeYlRmJKm3UHulSVA9wR76kBLf/wCI" +
            "jvG9elx23R6jBtSDT578IKp0VKn1pQ4pIy45yIJA/aAe/rqV9O90Xp1V9Pu7lpbiPVO4KXAp/wCK" +
            "Ui4pjfMRJjQWrwvFOConykDJwOY9Dohb52Z6YNjahN3TvNLU1dYlvuobqS1zUPPrUouBqMEnOCCO" +
            "4wn3I0Pe+fxJ6RMsGp2NtLaIt2lSmHIZqEhDbPhtOAhfhMN9kEgnCicjPp9MtRS2wyardnTTvjbs" +
            "ZhEpunQYdWZSWQpxoJkJLvBXAnugA45Y8n6fU6OOxuuXYywNtrOt+uVlMyqwaJCakpi09UhKXAyj" +
            "KOYGCod8jPY6XB07b+1Pp4u+oVuBFRUmZtNkU96BIVhh3mPL4iceYBYHbsSMjOCdVY64XXVuEJSp" +
            "ZJISAE/1AD0HtoJrznxNtiH6m0wizrgkkOhLMgUmJgEkAKGXuQH9mftold3lx34lJkLo1Sq61ocE" +
            "Viny5MYreXw4NqUwQEg9yVLISkIUe5xpFO3tPNVv62YITkyanGZx9eTqR/zp6+8KK0I9LbodQapz" +
            "rTbrviv1RUFCFp4BC14QvxEDJBbVhJJGc9sajNaorkm3NsHUs056S+zMS09GgKdqBfSX0pd8Mvnm" +
            "pPEq/UewGRnUKl3xekGNTo1GoFcpcZmI22uI3RGkNNOAd0tpOcJAxgZ1J/k0q2aqUWtwKtV30yCm" +
            "RHpE1b8594PpKVJdCWiDzwrASlKUjGOIwYJTLNjIjZrVDqbMhSstNJTVZDiGv2h1wFKS5654Dj6Y" +
            "zpBT/UhXKjU95rxhS58mTDp9answ47rqlNx0GS4pQbSThOVEk499FxsZs1tvuBV9orPZpEatz2Yz" +
            "dbr7YhpKUMLiFfJ58AFQdW61hJUccU8QnieQddQQ/p13AHv+PTR/vr/61DKdV6hSJQkwZsmFKTgB" +
            "6O6pCwBjHmB+38vprNbNO3l+FTZNxxZU7b6qSbUqfEqahSVfMQlH145PnQD9cqx9NCzXvhhb3UWI" +
            "/IYhUWrhpJV4cGojmvH7UhxKMnHfGqstLrB3nsjh+F7i1rgj0bmvCWn/AAvBQ0UHS38Q/dK8N3qD" +
            "b94y6RVKFOJblSn2mYPyyAlSlP8AiDiny8fQjvnt9dA6D+wqLVtvN9bSh1+mSqRUqfXoSn4c9lTT" +
            "qCJCD3SoD2B7/TGngbtU6PJjRJk2qzqPTYzLyZMqFOZjKCFFvsS8kpUMpB9QcgYzpWvXnupaN/dU" +
            "9NqlpSkz00tEWHMqDCgpmQ826pX5RB8wSDxKvcpwPTJaLuCwqdIt6W3+BeOy26+2uuxHJCUdm/Mj" +
            "gQEn07nv9PfTBX1SWGpe2vzNANQu0Sn0SWlVCSqG+/8AmoJUXQhJAAGR5cKCQO+dV1cl1VZuquMo" +
            "ueg2g4yODtNm3CuS6F5JKiceTsQOHtj76kl47o0mmbTMVO51Srmp1XfMZL9kx3lhaO6gU8V8gnyH" +
            "J5dz27g41XFpb9bf0+muRKZXYVtR2XlA0+4rdU1LQVYXkpQtAwQoYOO+tBN650KbH3LXKhWKpYzE" +
            "yoT31yZLypskeI4s5UcBwAZJ9teLfQNsG0kAbcwSB/HKkq/m5ogdZoxKBd6CthHkcTtzAA+qJMhJ" +
            "/wBHNc2R8PDYJ/0sVDXv+XUJI7/5n20SGs1Yg20z4d2w9MnCSLMMkp7hqRUJC0A/XHP/AN99dbqN" +
            "u1uhOUShO23eNbpK2zIfj2zT0yGHwk8Usvkn9Pvw7cu2SQCDfmgw6595L22zvK2olrXHKokeVT1u" +
            "vNx0NqC1hzAV50q747asQktmr3p24thw63THn3IjjjjQZlRUx3YykKKVMrQOwKCCNedzbA7fXjXJ" +
            "VYrVrQalU5JSXZL4UVKwkJH7vYADXG6ZbfZt3ZagFuRIlyKgFVGVJlrC3HX33CpxRIAHqT7atrSn" +
            "/9k=";

    public PDFFingerPrintStamper(byte[] pdfData) throws PDFReadingException
    {
        try
        {
            reader = new PdfReader(pdfData);
            pageHeight = reader.getPageSizeWithRotation(1).getHeight();
        }
        catch (IOException e)
        {
            throw new PDFReadingException(e);
        }
    }

    public byte[] writeFooterMessage(String message) throws FingerPrintAddException
    {
        try
        {
            ByteArrayOutputStream sout = new ByteArrayOutputStream();
            PdfStamper stamper = new PdfStamper(reader, sout);

            Font font = new Font(Font.HELVETICA, normalize(8), Font.NORMAL, new GrayColor(0.55f));

            int numberOfPages = reader.getNumberOfPages();

            for (int pageNumer = 1; pageNumer <= numberOfPages; pageNumer++)
            {
                PdfContentByte canvas = stamper.getOverContent(pageNumer);

                int index = 0;

                canvas.beginText();

                for (String line : message.split("\\n")) {
                    ColumnText.showTextAligned(canvas, Element.ALIGN_LEFT, new Paragraph(line, font), normalize(22) + (normalize(9)*index), normalize(60) , 90);
                    index = index + 1;
                }

                canvas.endText();

                Image img = Image.getInstance(Base64.getDecoder().decode(LOGO_UJI_IMG));
                //Image qr = Image.getInstance(new URL("file:/tmp/qr.jpg"));

                float w = normalize(60);
                float h = normalize(60);

                canvas.addImage(img, w/2, 0, 0, h/2, normalize(12), normalize(20));
                //canvas.addImage(qr, (w-5)/2, 0, 0, (h-5)/2, 12, 55);
            }

            stamper.close();

            return sout.toByteArray();

        }
        catch (Exception e)
        {
            throw new FingerPrintAddException(e);
        }
    }

    private float normalize(float value)
    {
        return (pageHeight*value)/836f;
    }
}