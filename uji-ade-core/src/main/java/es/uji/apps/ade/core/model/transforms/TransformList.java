package es.uji.apps.ade.core.model.transforms;

import es.uji.apps.ade.core.exceptions.InvalidTransformParameterException;
import es.uji.apps.ade.core.exceptions.StorageIOException;
import es.uji.apps.ade.core.exceptions.StorageUnavailableException;
import es.uji.apps.ade.core.model.Document;
import es.uji.apps.ade.core.model.domains.TransformType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransformList
{
    private Map<TransformType, List<TransformParam>> transforms;

    public TransformList()
    {
        this.transforms = new HashMap<>();
    }

    public void add(TransformType type, List<TransformParam> values)
    {
        transforms.put(type, values);
    }

    public Document apply(Document document) throws StorageIOException, InvalidTransformParameterException
    {
        List<Transform> transformList = buildTransforms();

        return transform(transformList, document);
    }

    private List<Transform> buildTransforms()
    {
        List<Transform> transformList = new ArrayList<>();

        for (TransformType transformType : transforms.keySet())
        {
            transformList.add(transformType.build(transforms.get(transformType)));
        }

        return transformList;
    }

    private Document transform(List<Transform> transformList, Document document)
            throws StorageIOException, InvalidTransformParameterException
    {
        Document result = document;

        for (Transform transform : transformList)
        {
            result = transform.apply(result);
        }

        return result;
    }
}
