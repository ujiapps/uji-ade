package es.uji.apps.ade.core.fulltext.exceptions;

public class IndexationException extends Exception
{
    public IndexationException(String message)
    {
        super(message);
    }
}
