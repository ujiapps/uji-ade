package es.uji.apps.ade.core.model.domains;

public enum DocumentStatus
{
    PENDIENTE_SUBIR("P"),
    SUBIDO("S"),
    PENDIENTE_BORRAR("B"),
    BORRADO("D");

    private String id;

    DocumentStatus(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
}
