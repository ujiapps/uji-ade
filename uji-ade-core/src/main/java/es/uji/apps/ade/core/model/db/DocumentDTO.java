package es.uji.apps.ade.core.model.db;

import es.uji.apps.ade.core.model.ReferenceGenerator;
import es.uji.apps.ade.core.model.domains.DocumentStatus;
import es.uji.apps.ade.core.model.domains.StorageType;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Table(name = "ADE_DOCUMENTOS")
@Entity
public class DocumentDTO
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FICHERO_NOMBRE")
    private String fileName;

    @Column(name = "FICHERO_MIMETYPE")
    private String mimeType;

    @Lob
    @Column(name = "FICHERO_CONTENIDO")
    private byte[] fileContents;

    @Column(name = "REFERENCIA")
    private String reference;

    @Column(name = "STORAGE_UID")
    private String storageUID;

    @Column(name = "STORAGE_PATH")
    private String storagePath;

    @Column(name = "ESTADO")
    private String statusId;

    @Column(name = "TIPO_ALMACENAMIENTO")
    private String storageTypeId;

    private String watermark;

    private String hash;

    @OneToMany(mappedBy = "document", cascade = CascadeType.ALL)
    private Set<DocumentMetadataDTO> metadata;

    public DocumentDTO()
    {
        this.storageTypeId = StorageType.FILESYSTEM.getId();
        this.storagePath = "/factum/DB";
        this.statusId = DocumentStatus.PENDIENTE_SUBIR.getId();

        ReferenceGenerator referenceGenerator = new ReferenceGenerator();
        this.reference = referenceGenerator.generate();
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public byte[] getFileContents()
    {
        return fileContents;
    }

    public String getReference()
    {
        return reference;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getStorageUID()
    {
        return storageUID;
    }

    public void setFileContents(byte[] fileContents)
    {
        this.fileContents = fileContents;
    }

    public String getStorageTypeId()
    {
        return storageTypeId;
    }

    public void setStorageUID(String storageUID)
    {
        this.storageUID = storageUID;
    }

    public void setStatusId(String statusId)
    {
        this.statusId = statusId;
    }

    public String getStatusId()
    {
        return statusId;
    }

    public void setStorageTypeId(String storageTypeId)
    {
        this.storageTypeId = storageTypeId;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getStoragePath()
    {
        return storagePath;
    }

    public void setStoragePath(String storagePath)
    {
        this.storagePath = storagePath;
    }

    public String getWatermark()
    {
        return watermark;
    }

    public void setWatermark(String watermark)
    {
        this.watermark = watermark;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Set<DocumentMetadataDTO> getMetadata()
    {
        Set<DocumentMetadataDTO> result = new HashSet<>();

        if (fileName != null)
            result.add(createMetadataField("_fileName", fileName));

        if (mimeType != null)
            result.add(createMetadataField("_mimeType", mimeType));

        if (reference != null)
            result.add(createMetadataField("_reference", reference));

        if (storageUID != null)
            result.add(createMetadataField("_storageUID", storageUID));

        if (storagePath != null)
            result.add(createMetadataField("_storagePath", storagePath));

        if (storageTypeId != null)
            result.add(createMetadataField("_storageTypeId", storageTypeId));

        result.addAll(metadata);

        return result;
    }

    private DocumentMetadataDTO createMetadataField(String key, String value)
    {
        DocumentMetadataDTO field = new DocumentMetadataDTO();
        field.setKey(key);
        field.setValue(value);

        return field;
    }


    public boolean isPDF()
    {
        if (mimeType == null) return false;

        return mimeType.toLowerCase().equals("application/pdf");
    }

    public void setMetadata(Set<DocumentMetadataDTO> metadata)
    {
        this.metadata = metadata;
    }
}