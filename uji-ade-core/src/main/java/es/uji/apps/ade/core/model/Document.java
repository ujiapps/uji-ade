package es.uji.apps.ade.core.model;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class Document
{
    protected String reference;
    protected String name;
    protected String uid;
    protected InputStream contents;
    protected String mimeType;
    protected String wartermark;
    protected Metadata metadata;

    public Document(String reference)
    {
        this.reference = reference;
        this.metadata = new Metadata();
    }

    public InputStream getContents()
    {
        return contents;
    }

    public void setContents(InputStream contents)
    {
        this.contents = contents;
    }

    public String getMimeType()
    {
        return mimeType;
    }

    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getUid()
    {
        return uid;
    }

    public void setUid(String uid)
    {
        this.uid = uid;
    }

    public String getWartermark()
    {
        return wartermark;
    }

    public void setWartermark(String wartermark)
    {
        this.wartermark = wartermark;
    }

    public void addMetadataAttribute(String key, String value)
    {
        this.metadata.addField(key, value);
    }

    public Document clone()
    {
        Document cloned = new Document(reference);
        initValues(cloned);

        return cloned;
    }

    protected void initValues(Document document)
    {
        document.setName(name);
        document.setUid(uid);
        document.setContents(contents);
        document.setMimeType(mimeType);
        document.setWartermark(wartermark);
    }

    public Map<String, List<String>> getRawMetadata()
    {
        return metadata.getRawValues();
    }

    public boolean hasWaterMark()
    {
        return getWartermark() != null && !getWartermark().isEmpty();
    }
}
