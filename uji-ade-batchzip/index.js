
import JSZip from "jszip";
import sanitize from "sanitize-filename";
import fs from "fs";

const fsPromises = fs.promises;

import {
  getConnection,
  retrieveJobs,
  retrieveJobsReferences,
  markJobAsCompleted,
} from "./repositories/jobs-repository.js";

import { retrieveContent, saveJobContent } from "./repositories/content-repository.js";


async function processJob(db, jobID) {
  const zip = new JSZip();

  console.log(`Processing job ${jobID}...`);

  for (let {
    REFERENCIA: key,
    FILENAME: filename,
  } of await retrieveJobsReferences(db, jobID)) {
    const content = await retrieveContent(key);

    try {
      zip.file(sanitize(filename), content, { binary: true });
    } catch (e) {
      console.log(`Can't export ${filename}`);
    }
  }

  return zip
    .generateAsync({ type: "nodebuffer", streamFiles: true })
    .then((content) => fsPromises.writeFile(`${jobID}.zip`, content));
}

(async function () {
  const db = await getConnection();

  for (let { ID: jobID } of await retrieveJobs(db)) {
    await processJob(db, jobID);

    const url = await saveJobContent(jobID);

    await markJobAsCompleted(db, jobID, url);
  }

  process.exit();
})();
