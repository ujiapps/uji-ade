import oracledb from "oracledb";
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

export async function getConnection() {
  return await oracledb.getConnection({
    user: "uji_intermediacion",
    password: "scrum2010",
    connectString: "db.n.uji.es/alma",
  });
}

export async function retrieveJobs(db) {
  const result = await db.execute(
    `SELECT id
       FROM ade_exportaciones_pendientes
      WHERE url is null`
  );

  return result.rows;
}

export async function retrieveJobsReferences(db, jobID) {
  const result = await db.execute(
    `SELECT referencia, filename
       FROM ade_exportaciones_pendientes_referencias
      WHERE exportacion_id = ${jobID}`
  );

  return result.rows;
}

export async function markJobAsCompleted(db, id, url) {
  await db.execute(
    `UPDATE ade_exportaciones_pendientes 
        SET url = :url 
      WHERE id = :id`,
    { id, url },
    { autoCommit: true }
  );
}
