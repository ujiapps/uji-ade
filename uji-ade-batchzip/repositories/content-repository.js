import fs from "fs";
import fetch from "node-fetch";

import { S3, GetObjectCommand } from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";

const fsPromises = fs.promises;

const DOCUMENTS_DIR = "/mnt/dbobjects/alma";
const URL_ADE = "https://ujiapps.uji.es/ade/rest/storage";

const SPOOL_BUCKET = "uji-spool";
const A_MONTH = 30 * 24 * 60 * 60;

function keyToFileName(key) {
  return `${DOCUMENTS_DIR}/${key.substring(0, 2)}/${key.substring(
    2,
    4
  )}/${key}`;
}

export async function retrieveContent(key) {
  let data;

  try {
    await fsPromises.access(DOCUMENTS_DIR);

    console.log("[FS]", key);

    const fileName = keyToFileName(key);
    data = await fsPromises.readFile(fileName);
  } catch (e) {
    console.log("[WEB]", key);

    const request = await fetch(`${URL_ADE}/${key}`);

    const arrayBuffer = await request.arrayBuffer();
    data = Buffer.from(arrayBuffer);
  }

  return data;
}

export async function saveJobContent(jobID) {
  const fileName = `${jobID}.zip`;
  const data = await fsPromises.readFile(fileName);

  const client = new S3({ region: "eu-west-1" });

  await client.putObject({
    Bucket: SPOOL_BUCKET,
    Key: fileName,
    Body: data,
    ContentType: "application/zip",
  });

  return getSignedUrl(
    client,
    new GetObjectCommand({
      Bucket: SPOOL_BUCKET,
      Key: fileName,
      Expires: A_MONTH,
    }),
    { expiresIn: 3600 }
  );
}
