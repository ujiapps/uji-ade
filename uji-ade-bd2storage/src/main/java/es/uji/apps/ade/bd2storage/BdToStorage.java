package es.uji.apps.ade.bd2storage;

import es.uji.apps.ade.core.storage.DocumentPersister;
import es.uji.apps.ade.core.storage.IndexationClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

public class BdToStorage implements Runnable
{
    private static Logger log = LoggerFactory.getLogger(BdToStorage.class);

    private static final int WAIT_TIME = 1 * 1000;

    private DocumentPersister persister;
    private IndexationClient indexationClient;

    public BdToStorage(DocumentPersister persister, IndexationClient indexationClient)
    {
        this.persister = persister;
        this.indexationClient = indexationClient;
    }

    public static void main(String[] args) throws Exception
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        DocumentPersister persister = context.getBean(DocumentPersister.class);
        IndexationClient indexationClient = context.getBean(IndexationClient.class);

        log.info("START BdToStorage " + new Date());

        new Thread(new BdToStorage(persister, indexationClient)).start();
    }

    public void run()
    {
        while (true)
        {
            try
            {
                persister.savePendingDocumentsToStorage();
                persister.removeExpiredDocuments();

                waitUntil(WAIT_TIME);
            }
            catch (Exception e)
            {
                log.error("Can't read from database");
                waitUntil(WAIT_TIME);
            }
        }
    }

    private void waitUntil(int amount)
    {
        try
        {
            Thread.sleep(amount);
        }
        catch (InterruptedException e)
        {
            log.error("Error processing message queue", e);
        }
    }
}