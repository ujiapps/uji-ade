package es.uji.apps.ade.bd2storage.alfresco;

import java.io.InputStream;
import java.util.Date;

import es.uji.apps.ade.core.model.domains.DocumentStatus;
import es.uji.apps.ade.core.model.domains.StorageType;
import es.uji.commons.rest.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.uji.apps.ade.core.dao.DatabaseDocumentDAO;
import es.uji.apps.ade.core.model.db.DocumentDTO;
import es.uji.apps.ade.core.storage.Storage;
import es.uji.apps.ade.core.storage.StorageFactory;

public class MigrateAlfrescoToFilesystem implements Runnable
{
    private static Logger log = LoggerFactory.getLogger(MigrateAlfrescoToFilesystem.class);

    private DatabaseDocumentDAO databaseDocumentDAO;

    public MigrateAlfrescoToFilesystem(DatabaseDocumentDAO databaseDocumentDAO)
    {
        this.databaseDocumentDAO = databaseDocumentDAO;
    }

    public static void main(String[] args) throws Exception
    {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "classpath:applicationContext.xml");

        DatabaseDocumentDAO databaseDocumentDAO = context.getBean(DatabaseDocumentDAO.class);
        log.info("START MigrateAlfrescoToFilesystem " + new Date());

        new Thread(new MigrateAlfrescoToFilesystem(databaseDocumentDAO)).start();
    }

    public void run()
    {
        for (DocumentDTO document : databaseDocumentDAO.getMigratedAlfrescoDocuments())
        {
            Storage storage = StorageFactory.build(document.getStorageTypeId());

            try
            {
                InputStream dataStream = storage.getByReference(document.getStorageUID());

                document.setFileContents(StreamUtils.inputStreamToByteArray(dataStream));
                document.setStorageUID("");
                document.setStorageTypeId(StorageType.FILESYSTEM.getId());
                document.setStatusId(DocumentStatus.PENDIENTE_SUBIR.getId());

                dataStream.close();

                databaseDocumentDAO.update(document);

                log.info("Migrating file " + document.getId() + " " + document.getFileName());
            }
            catch (Exception e)
            {
                log.error("Error migrating document: ", e);
            }
        }
    }
}
